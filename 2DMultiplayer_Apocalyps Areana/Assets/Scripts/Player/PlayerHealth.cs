﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Object = UnityEngine.Object;
using UnityEngine.Analytics;

public class PlayerHealth : MonoBehaviourPun, IDamageAble
{
    [SerializeField] float health = 100;
    [SerializeField] Image healthFillImage = null;
    [SerializeField] Image healthFillImageNetwork = null;

    [Space]
    [SerializeField] SpriteRenderer[] playerSpriteRenderer = null;

    [Header("Dead")]
    [SerializeField] GameObject killFeedObj = null;
    [SerializeField] Transform deadObj = null;
    [SerializeField] Animator playerAnim = null;

    [Header("Flash on Damage")]

    [SerializeField] Color flashColor = Color.red;
    [SerializeField] float flashSpeed = 12f;
    [SerializeField] GameObject bloodHitEffect = null;
    [SerializeField] GameObject dieEffect = null;
    [SerializeField] Vector3 dieEffectOffSet = Vector3.zero;

    public delegate void DelegatePlayerDie(string killerName);
    public event DelegatePlayerDie EventOnPlayerDie;

    private PlayerMovement playerMovement;
    private string killerObjName;
    private GameObject killerObj;
    private Player lastKiller;

    private float currentHealth;
    public float GetCurrentHealth { get { return currentHealth; } }
    private bool isTakeDamage;

    private string killerName; //track last player's bullet that was hit

    private void Start()
    {
        if (PhotonNetwork.OfflineMode)
        {
            RPCInitPlayerHealth();
        }
        if (photonView.IsMine)
        {
            EventOnPlayerDie += OnLocalPlayerHasBeenDead;
            EventOnPlayerDie += GameplayManager.Instance.OnLocalPlayerDie;

            photonView.RPC("RPCInitPlayerHealth", RpcTarget.All);
        }
    }

    private void Update()
    {
        OnDamaged();

        if (Input.GetKeyDown(KeyCode.Q))
        {
            currentHealth = 0;

            playerMovement.SetDisableInput(true);
            EventOnPlayerDie(killerName);
        }
    }

    void OnDamaged()
    {
        if (isTakeDamage)
        {
            foreach (SpriteRenderer sr in playerSpriteRenderer)
            {
                sr.color = flashColor;
            }

            isTakeDamage = false;
        }
        else
        {
            foreach (SpriteRenderer sr in playerSpriteRenderer)
            {
                sr.color = Color.Lerp(sr.color, Color.white, flashSpeed * Time.deltaTime);
            }

        }
    }

    public void OnLocalPlayerHasBeenDead(string killerName)
    {
        CameraFollow2D.Instance.SetTarget(killerObj);
        CameraFollow2D.Instance.isActiveOffset = false;

        if (PhotonNetwork.OfflineMode)
        {
            RPCPlayerDead(killerName);
        }
        else
        {
            photonView.RPC("RPCPlayerDead", RpcTarget.All, killerName);
        }
    }

    public void TakeDamage(float value, string killerName, string killerCharObjName, Player playerKiller)
    {
        if (PhotonNetwork.OfflineMode)
        {
            RPCTakeDamage(value, killerName, killerCharObjName);
        }
        else
        {
            photonView.RPC("RPCTakeDamage", RpcTarget.AllBuffered, value, killerName, killerCharObjName);
        }

        if (playerKiller != null)
        {
            lastKiller = playerKiller;
        }

    }

    public void AddHealth(float value)
    {
        if (PhotonNetwork.OfflineMode)
        {
            RPCAddHealth(value);
        }
        else
        {
            photonView.RPC("RPCAddHealth", RpcTarget.All, value);
        }
    }

    public bool CheckIsDied()
    {
        if (currentHealth <= 0)
        {
            if (photonView.IsMine)
            {
                playerMovement.SetDisableInput(true);
                EventOnPlayerDie(killerName);
            }

            return true;
        }

        return false;
    }

    void UpdateHealthImage()
    {
        if (photonView.IsMine || PhotonNetwork.OfflineMode)
        {
            healthFillImage.fillAmount = currentHealth / health;
        }
        else
        {
            healthFillImageNetwork.fillAmount = currentHealth / health;
        }
    }

    #region  PunRPC

    [PunRPC]
    void RPCTakeDamage(float value, string bulletOwnerName, string killerCharObjName)
    {
        if (currentHealth <= 0)
        {
            return;
        }

        currentHealth -= value;
        killerName = bulletOwnerName;

        isTakeDamage = true;

        GameObject effect = Instantiate(bloodHitEffect);
        effect.transform.SetParent(transform, false);
        effect.transform.localPosition = Vector3.zero;
        Destroy(effect, 1.5f);

        UpdateHealthImage();

        if (currentHealth <= 0)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

            foreach (var player in players)
            {
                if (player.name == killerCharObjName)
                {
                    killerObj = player;
                }
            }

            if (killerObj == null)
            {
                GameObject[] allZombie = GameObject.FindGameObjectsWithTag("Zombie");

                foreach (var zombie in allZombie)
                {
                    if (zombie.name == killerCharObjName)
                    {
                        killerObj = zombie;
                    }
                }
            }
        }

        CheckIsDied();

    }

    [PunRPC]
    void RPCPlayerDead(string killerName)
    {
        GameObject feedText = Instantiate(killFeedObj, Vector3.zero, Quaternion.identity);

        GameObject effect = Instantiate(dieEffect);
        effect.transform.SetParent(transform, false);
        effect.transform.localPosition = Vector3.zero + dieEffectOffSet;

        if (GameplayManager.Instance != null)
        {
            if (GameplayManager.Instance.IsInstanceExit && feedText != null)
                feedText.transform.SetParent(GameplayManager.Instance.KillFeedUi.transform, false);

            TextMeshProUGUI textMesh = feedText.GetComponentInChildren<TextMeshProUGUI>();

            if (!PhotonNetwork.OfflineMode && textMesh != null)
                textMesh.text = photonView.Owner.NickName + " Killed by " + killerName;
        }

        playerAnim.enabled = false;

        if (deadObj != null)
        {
            deadObj.gameObject.SetActive(true);

            foreach (SpriteRenderer sr in playerSpriteRenderer)
            {
                sr.gameObject.SetActive(false);
            }
        }

        GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        GetComponent<Rigidbody2D>().isKinematic = true;
        GetComponent<Collider2D>().enabled = false;

        if (lastKiller != null && lastKiller != PhotonNetwork.LocalPlayer)
        {
            GameScoreManager.Instance.AddPlayerScore(80);
        }

        Destroy(gameObject, 5);
    }

    [PunRPC]
    void RPCAddHealth(float value)
    {
        currentHealth += value;

        if (currentHealth > health)
        {
            currentHealth = health;
        }

        UpdateHealthImage();
    }

    [PunRPC]
    void RPCInitPlayerHealth()
    {
        currentHealth = health;
        isTakeDamage = false;

        playerMovement = GetComponent<PlayerMovement>();
        UpdateHealthImage();
    }

    #endregion
}