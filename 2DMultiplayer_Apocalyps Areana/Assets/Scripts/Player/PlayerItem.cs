﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;

public class PlayerItem : MonoBehaviourPun
{
    [SerializeField] Vector2 detectOffset = Vector2.zero;
    [SerializeField] float detectRadius = 0;
    [SerializeField] float minDetectRange = 0;
    [SerializeField] float disDetectRange = 0;
    [SerializeField] LayerMask itemLayer = 0;

    [Header("Ui")]
    [SerializeField] GameObject buttonTakeItem = null;

    [Header("Consumable Item")]
    [SerializeField] GameObject addHealthEffectPrefab = null;
    [SerializeField] TextMeshProUGUI medicCountText = null;
    [SerializeField] TextMeshProUGUI specialItemCountText = null;
    [SerializeField] GameObject specialItemPrefab = null;

    private int medicCount;
    private int specialItemCount;

    List<Collider2D> contactItems = new List<Collider2D>();
    PlayerShoot playerWeapon;
    PlayerHealth playerHealth;
    GameObject closetItem;

    Transform selector;

    bool isFaceOnRightSide;
    bool isDisableUpdate;

    void Start()
    {
        isDisableUpdate = false;

        selector = GameplayManager.Instance.ItemSelector;
        playerWeapon = GetComponent<PlayerShoot>();
        playerHealth = GetComponent<PlayerHealth>();

        playerHealth.EventOnPlayerDie += OnPlayerDie;

        medicCount = 1;
        specialItemCount = 1;
        UpdateItemAmount();
    }

    void FixedUpdate()
    {
        if (!photonView.IsMine || isDisableUpdate) return;

        UpdateSelectedItem();
    }

    void UpdateSelectedItem()
    {
        int faceMultiplier = isFaceOnRightSide ? 1 : -1;
        Collider2D[] contactItem = Physics2D.OverlapCircleAll((Vector2) transform.position + (detectOffset * faceMultiplier), detectRadius, itemLayer);

        for (int i = 0; i < contactItem.Length; i++)
        {
            if (contactItem != null && !contactItems.Contains(contactItem[i]) && !contactItem[i].GetComponent<ItemBase>().ignoreThisitem)
                contactItems.Add(contactItem[i]);
        }

        if (contactItems.Count > 0)
        {
            if (closetItem != null) //close old highlight
            {
                closetItem.GetComponent<ItemBase>().HighlightActive(false);
            }

            if (contactItems.Count == 1)
            {
                if (contactItems[0] == null)
                {
                    contactItems.RemoveAt(0);
                }
                else
                {
                    closetItem = contactItems[0].gameObject;
                }

            }
            else if (contactItems.Count >= 2)
            {
                closetItem = GetClosetItem(contactItems, transform.position);
            }

            for (int i = 0; i < contactItems.Count; i++)
            {
                Vector3 dis = contactItems[i].transform.position - transform.position;

                if (dis.sqrMagnitude > disDetectRange || contactItems[i].GetComponent<ItemBase>().ignoreThisitem)
                {
                    contactItems.RemoveAt(i);
                }
            }

            if (closetItem != null)
            {
                buttonTakeItem.SetActive(true);
                selector.gameObject.SetActive(true);
                selector.transform.position = closetItem.transform.GetChild(0).position;
                closetItem.GetComponent<ItemBase>().HighlightActive(true);
            }
        }
        else
        {
            if (closetItem != null)
            {
                closetItem.GetComponent<ItemBase>().HighlightActive(false);
                closetItem = null;
            }

            buttonTakeItem.SetActive(false);
            selector.gameObject.SetActive(false);
        }
    }

    public void IsSwapFace(bool value)
    {
        isFaceOnRightSide = !value;
    }

    GameObject GetClosetItem(List<Collider2D> stuffs, Vector3 playerPos)
    {
        Transform closetTarget = null;

        if (closetTarget != null)
            closetTarget = closetItem.transform;

        float closetDistanceSqr = Mathf.Infinity;
        Vector3 currentPosition = playerPos;

        foreach (Collider2D stuff in stuffs)
        {
            if (stuff == null)
            {
                stuffs.Remove(stuff);
                continue;
            }

            Vector3 distance = stuff.transform.position - currentPosition;
            float manhattanDistance = distance.sqrMagnitude;

            if (manhattanDistance < closetDistanceSqr)
            {
                if (closetTarget == null)
                {
                    closetDistanceSqr = manhattanDistance;
                    closetTarget = stuff.transform;
                }
                else
                {
                    if (manhattanDistance > minDetectRange)
                    {
                        closetDistanceSqr = manhattanDistance;
                        closetTarget = stuff.transform;
                    }
                }

            }
        }

        if (closetTarget != null)
            return closetTarget.gameObject;
        else
            return null;
    }

    public void OnPlayerDie(string killerName)
    {
        if (closetItem != null)
        {
            closetItem.GetComponent<ItemBase>().HighlightActive(false);
            closetItem = null;
        }

        if (buttonTakeItem != null)
            buttonTakeItem.SetActive(false);

        if (selector != null)
            selector.gameObject.SetActive(false);

        isDisableUpdate = true;
    }

    public void OnBtnPickItem()
    {
        if (closetItem == null)
        {
            UpdateSelectedItem();
            return;
        }

        ItemBase currentSelectItem = closetItem.gameObject.GetComponent<ItemBase>();
        GameObject data = null;

        if (currentSelectItem.GetSentData != null)
            data = (GameObject) currentSelectItem.GetSentData;

        switch (currentSelectItem.GetItemType)
        {
            case ItemBase.ItemType.Weapon:
                if (data != null)
                    playerWeapon.AddNewWeapon(data.GetComponent<WeaponBase>());
                break;

            case ItemBase.ItemType.Medic:
                medicCount++;

                if (medicCount > 3)
                {
                    medicCount = 3;
                }

                UpdateItemAmount();
                break;

            case ItemBase.ItemType.Special:
                specialItemCount++;

                if (specialItemCount > 3)
                {
                    specialItemCount = 3;
                }

                UpdateItemAmount();
                break;

            case ItemBase.ItemType.Looting:
                currentSelectItem.GetComponent<LootableObject>().OpenLoot();
                break;
        }

        contactItems.Remove(closetItem.GetComponent<Collider2D>());
        closetItem.GetComponent<ItemBase>().DestroyThisItem();
        closetItem = null;
    }

    void UpdateItemAmount()
    {
        medicCountText.text = medicCount.ToString() + "/3";
        specialItemCountText.text = specialItemCount.ToString() + "/3";
    }

    #region  Ui Reference

    public void OnBtnUseMedicItem()
    {
        if (medicCount > 0 && playerHealth.GetCurrentHealth < 100)
        {
            playerHealth.AddHealth(60);
            medicCount--;
            UpdateItemAmount();

            if(PhotonNetwork.OfflineMode)
            {
                RPCPlayAddHealthEffect();
            }
            else
            {
                photonView.RPC("RPCPlayAddHealthEffect", RpcTarget.All);
            }

            Analytics.CustomEvent("Player use medic Count");
        }
    }

    public void OnBtnUseSpecial()
    {
        if (specialItemCount > 0)
        {
            GameObject newObj = PhotonNetwork.Instantiate("Gadget/" + specialItemPrefab.name, transform.position, Quaternion.identity);
            newObj.GetComponent<BombController>().ThrowBomb(transform.right + Vector3.up, PhotonNetwork.LocalPlayer, gameObject.name);

            specialItemCount--;
            UpdateItemAmount();

            Analytics.CustomEvent("Player use grenade Count");
        }
    }

    #endregion 

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        int faceMultiplier = isFaceOnRightSide ? 1 : -1;
        Gizmos.DrawWireSphere((Vector2) transform.position + (detectOffset * faceMultiplier), detectRadius);
    }

    [PunRPC]
    void RPCPlayAddHealthEffect()
    {
        Instantiate(addHealthEffectPrefab, transform.position, Quaternion.Euler(-90,0,0));
    }

}