﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShoot : MonoBehaviourPun
{
    [SerializeField] Transform weaponHoding = null;
    [SerializeField] Image fireRateCooldownFillImage = null;

    [Header("Holding Weapon")]
    [SerializeField] PlayerHodingWeapon firstCarryingWeapon = null;
    [SerializeField] PlayerHodingWeapon secondCarryingWeapon = null;

    [Header("Reloading Ui")]
    [SerializeField] Image reloadFillImage = null;
    [SerializeField] TextMeshProUGUI reloadText = null;
    [SerializeField] GameObject reloadScreen = null;

    [Header("Weapon Swap Ui")]
    [SerializeField] Image nextWeaponImage = null;
    [SerializeField] Image swapFillImage = null;
    [SerializeField] GameObject swapWeaponUi = null;

    [Header("Sound")]
    [SerializeField] AudioSource personalSoundSource = null;
    [SerializeField] GameAudioInfo beginSwapGunSound = null;
    [SerializeField] GameAudioInfo endSwapGunSound = null;

    private PlayerHodingWeapon currentCarryingWeapon = new PlayerHodingWeapon();
    private PlayerHodingWeapon previousCarryingWeapon = new PlayerHodingWeapon();
    private WeaponBase currentWeapon;
    private WeaponBase previousWeapon;
    private PlayerBase playerBase;
    private bool isWeaponHasBeenSwap;
    private bool isStartSwapWeapon;
    private bool isDisablePlayer;
    private bool isOnBtnShoot;

    private float weaponSwapTimer;

    private const float WEAPON_SWAP_TIME = 1f;

    void Awake()
    {
        playerBase = GetComponent<PlayerBase>();

        isWeaponHasBeenSwap = false;
        isStartSwapWeapon = false;
        isDisablePlayer = false;
        isOnBtnShoot = false;

        weaponSwapTimer = 0;
    }

    void Start()
    {
        InitPlayerWeapon();

        if (!photonView.IsMine) return;

        GetComponent<PlayerHealth>().EventOnPlayerDie += ClearAllWeaponNetworkVisualized;

    }

    private void Update()
    {
        if (!photonView.IsMine || isDisablePlayer) return;

        if (currentWeapon == null)
        {
            if (reloadScreen != null)
                reloadScreen.SetActive(false);

            if (swapWeaponUi != null)
                swapWeaponUi.SetActive(false);

            return;
        }

        UpdateBtnDelayReloadWeapon();
        UpdateDelaySwapWeapon();
        UpdateFirerateCooldown();
        UpdateFireBtn();
    }

    #region Update Function

    private void UpdateBtnDelayReloadWeapon()
    {
        if (currentWeapon.IsReloading) //Weapon reloading count will occur in Weaponbase Class
        {
            reloadScreen.SetActive(true);
            currentWeapon.UpdateReloadUi(reloadFillImage, reloadText);
        }
        else
        {
            reloadScreen.SetActive(false);
        }
    }

    private void UpdateFirerateCooldown()
    {
        currentWeapon.UpdateFirerateImage(fireRateCooldownFillImage);
    }

    private void UpdateDelaySwapWeapon()
    {
        if (isStartSwapWeapon)
        {
            swapWeaponUi.SetActive(true);

            weaponSwapTimer += Time.deltaTime;
            swapFillImage.fillAmount = weaponSwapTimer / WEAPON_SWAP_TIME;

            if (weaponSwapTimer >= WEAPON_SWAP_TIME)
            {
                SwapCurrentHoldingWeapon();
                isStartSwapWeapon = false;
            }
        }
        else
        {
            swapWeaponUi.SetActive(false);
        }
    }

    private void UpdateFireBtn()
    {
        if (isOnBtnShoot)
        {
            if (currentWeapon.ShootBullet())
            {
                currentCarryingWeapon.UpdateUI();
            }
            else if (currentWeapon.GetCurrentAmmo <= 0)
            {
                if (PhotonNetwork.OfflineMode)
                {
                    RpcPlayEmptyBulletSound();
                }
                else
                {
                    photonView.RPC("RpcPlayEmptyBulletSound", RpcTarget.All);
                }

            }

            isStartSwapWeapon = false;

            if (currentWeapon.GetCurrentAmmo <= 0)
            {
                isOnBtnShoot = false;
            }
        }

    }

    #endregion

    private void InitPlayerWeapon()
    {
        if (firstCarryingWeapon.weapon != null && firstCarryingWeapon != null)
        {
            InitializeWeapon(firstCarryingWeapon.weapon, firstCarryingWeapon);
            currentWeapon = SetWeaponCarryingState(firstCarryingWeapon, currentCarryingWeapon);

            currentCarryingWeapon.SetHoldingWeaponActive(true);
        }
        if (secondCarryingWeapon.weapon != null && secondCarryingWeapon != null)
        {
            InitializeWeapon(secondCarryingWeapon.weapon, secondCarryingWeapon);
            previousWeapon = SetWeaponCarryingState(secondCarryingWeapon, previousCarryingWeapon);

            previousCarryingWeapon.SetHoldingWeaponActive(false);
        }

        if (photonView.IsMine)
        {
            firstCarryingWeapon.UpdateUI();
            secondCarryingWeapon.UpdateUI();
        }
    }

    WeaponBase SetWeaponCarryingState(PlayerHodingWeapon targetCarryingSlot, PlayerHodingWeapon carryingState)
    {
        carryingState.weapon = targetCarryingSlot.weapon;
        carryingState.weaponImage = targetCarryingSlot.weaponImage;
        carryingState.holdingObj = targetCarryingSlot.holdingObj;
        carryingState.textAmmoCount = targetCarryingSlot.textAmmoCount;
        carryingState.arrowImage = targetCarryingSlot.arrowImage;

        return carryingState.weapon;
    }

    public void AddNewWeapon(WeaponBase newWeapon)
    {
        if (PhotonNetwork.OfflineMode)
        {
            RpcAddNewWeapon(newWeapon.name);
        }
        else
        {
            photonView.RPC("RpcAddNewWeapon", RpcTarget.All, newWeapon.name);
        }
    }

    private void InitializeWeapon(WeaponBase newWeapon, PlayerHodingWeapon weaponCarryingSlot)
    {
        if (newWeapon == null || weaponCarryingSlot == null) return;

        GameObject weaponObj = null;

        weaponObj = Instantiate(newWeapon.gameObject, Vector3.zero, Quaternion.identity);

        if (weaponObj == null) return;

        weaponObj.transform.SetParent(weaponCarryingSlot.holdingObj.transform, false);

        WeaponBase weapon = weaponObj.GetComponent<WeaponBase>();
        if (weapon != null && weaponCarryingSlot != null)
        {
            weapon.SetHoldingSlot(weaponCarryingSlot, this); //We need assign carrying slot for callback

            weaponCarryingSlot.weapon = weapon;
            weaponCarryingSlot.SetHoldingWeaponActive(false);

            if (photonView.IsMine)
            {
                weaponCarryingSlot.UpdateUI();
            }
            else
            {
                UpdateWeaponSpriteLayer(weapon.GetComponentInChildren<SpriteRenderer>());
            }
        }
    }

    private void ClearAllWeaponNetworkVisualized(string killerName)
    {
        if (PhotonNetwork.OfflineMode)
        {
            RpcClearAllWeaponVisualized();
        }
        else
        {
            photonView.RPC("RpcClearAllWeaponVisualized", RpcTarget.All);
        }

        isDisablePlayer = true;
    }

    private void SwapCurrentHoldingWeapon()
    {

        if (PhotonNetwork.OfflineMode)
        {
            RpcSwapCarryingWeapon();
            RpcPlayEndSwapWeaponSound();
        }
        else
        {
            photonView.RPC("RpcSwapCarryingWeapon", RpcTarget.All);
            photonView.RPC("RpcPlayEndSwapWeaponSound", RpcTarget.All);
        }

    }

    #region UI

    public void OnBtnShootEnter()
    {
        if (currentWeapon == null) return;

        isOnBtnShoot = true;
    }

    public void OnBtnShootRelease()
    {
        if (currentWeapon == null) return;

        isOnBtnShoot = false;
    }

    public void OnBtnReload()
    {
        if (currentWeapon == null) return;

        if (currentWeapon.Reload())
        {
            isStartSwapWeapon = false;
        }
    }

    public void OnBtnSwapWeapon()
    {
        if (isStartSwapWeapon || currentWeapon == null || previousWeapon == null) return;

        if (PhotonNetwork.OfflineMode)
        {
            RpcPlayBeginSwapWeaponSound();
        }
        else
        {
            photonView.RPC("RpcPlayBeginSwapWeaponSound", RpcTarget.All);
        }

        isStartSwapWeapon = true;

        nextWeaponImage.sprite = previousWeapon.GetWeaponSprite;
        currentWeapon.CancelReload();

        weaponSwapTimer = 0;
    }

    void UpdateWeaponSpriteLayer(SpriteRenderer targetSprite)
    {
        if (targetSprite == null)
        {
            return;
        }

        targetSprite.sortingLayerName = "MyEnemy";
        targetSprite.sortingOrder = 1;
    }

    #endregion

    #region  PunRPC

    [PunRPC]
    void RpcClearAllWeaponVisualized()
    {
        weaponHoding.gameObject.SetActive(false);
    }

    [PunRPC]
    void RpcSwapCarryingWeapon()
    {
        if (firstCarryingWeapon == null || secondCarryingWeapon == null || currentCarryingWeapon == null) return;

        previousWeapon = SetWeaponCarryingState(currentCarryingWeapon, previousCarryingWeapon); //remember previous weapon

        if (isWeaponHasBeenSwap) //Toggle beetween first weapon and second weapon
        {
            if (firstCarryingWeapon.weapon != null)
            {
                currentWeapon = SetWeaponCarryingState(firstCarryingWeapon, currentCarryingWeapon);
                isWeaponHasBeenSwap = false;
            }
            else
            {
                isWeaponHasBeenSwap = true;
            }
        }
        else
        {
            if (secondCarryingWeapon.weapon != null)
            {
                currentWeapon = SetWeaponCarryingState(secondCarryingWeapon, currentCarryingWeapon);
                isWeaponHasBeenSwap = true;
            }
            else
            {
                isWeaponHasBeenSwap = false;
            }
        }

        previousCarryingWeapon.SetHoldingWeaponActive(false);
        currentCarryingWeapon.SetHoldingWeaponActive(true);

        if (photonView.IsMine)
        {
            currentWeapon.CancelReload();
            currentCarryingWeapon.UpdateUI();
        }
    }

    [PunRPC]
    public void RpcAddNewWeapon(string newWeaponName)
    {
        WeaponBase newWeapon = Resources.Load<WeaponBase>(newWeaponName);

        if (secondCarryingWeapon.weapon == null && currentWeapon != null)
        {
            InitializeWeapon(newWeapon, secondCarryingWeapon);
            previousWeapon = SetWeaponCarryingState(secondCarryingWeapon, previousCarryingWeapon);
        }
        else
        {
            if (currentWeapon != null)
            {
                currentWeapon.DestroyWeapon();
            }

            if (!isWeaponHasBeenSwap)
            {
                InitializeWeapon(newWeapon, firstCarryingWeapon);
                currentWeapon = SetWeaponCarryingState(firstCarryingWeapon, currentCarryingWeapon);
            }
            else
            {
                InitializeWeapon(newWeapon, secondCarryingWeapon);
                currentWeapon = SetWeaponCarryingState(secondCarryingWeapon, currentCarryingWeapon);
            }
        }

        currentCarryingWeapon.SetHoldingWeaponActive(true);
    }

    [PunRPC]
    public void RpcPlayBeginSwapWeaponSound()
    {
        personalSoundSource.PlayOneShot(beginSwapGunSound.audioClip, beginSwapGunSound.audioVolume);
    }

    [PunRPC]
    public void RpcPlayEndSwapWeaponSound()
    {
        personalSoundSource.PlayOneShot(endSwapGunSound.audioClip, endSwapGunSound.audioVolume);
    }

    [PunRPC]
    public void RpcInitializeWeapon(string weaponName)
    {
        
    }

    #region Call on WeaponBase

    [PunRPC]
    public void RpcWeaponOnShoot()
    {
        currentWeapon.OnWeaponShoot();
    }

    [PunRPC]
    public void RpcPlayReloadSound()
    {
        currentWeapon.PlayReloadSound();
    }

    [PunRPC]
    public void RpcPlayEmptyBulletSound()
    {
        currentWeapon.PlayEmptyBulletSound();
    }

    #endregion

    #endregion
}