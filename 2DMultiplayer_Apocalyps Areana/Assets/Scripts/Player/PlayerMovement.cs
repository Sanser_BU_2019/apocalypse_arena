﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMovement : MonoBehaviourPun
{
    [SerializeField] GameObject controllerObj = null;
    [SerializeField] VariableJoystick joystick = null;

    [Header("Stats")]
    [SerializeField] float runSpeed = 20;

    [Header("Jump")]
    [SerializeField] float jumpVelocity = 5;
    [SerializeField] float fallMultiplier = 2.5f;
    [SerializeField] float lowJumpMultiplier = 2f;
    [SerializeField] LayerMask groundLayer = 0;
    [SerializeField] LayerMask platformLayer = 15;
    [SerializeField] Vector2 bottomOffset = Vector2.zero;
    [SerializeField] float collisionRadius = 1f;
    [SerializeField] ParticleSystem jumpEffect = null;

    [Header("Sound")]
    [SerializeField] AudioSource footSoundSource = null;
    [SerializeField] AudioSource personalSoundSource = null;
    [SerializeField] GameAudioInfo jumpSound = null;

    private PlayerItem playerItem;
    private PlayerBase playerBase;
    private Rigidbody2D rb;
    private Animator anim;
    private bool isDisableInput;
    private bool isOnFloor;
    private bool isBtnJumpPressed;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
        playerBase = GetComponent<PlayerBase>();
        playerItem = GetComponent<PlayerItem>();

        isDisableInput = false;
        isBtnJumpPressed = false;
        isOnFloor = true;
    }

    private void FixedUpdate()
    {
        UpdateCheckFloor();

        if (!photonView.IsMine || isDisableInput)
        {
            GetComponent<PlayerMovement>().enabled = false;
            Destroy(controllerObj);
            return;
        }

        UpdateRunning();
        UpdateJump();
    }

    #region  Update

    void UpdateRunning()
    {
        float runVelocity = joystick.Horizontal * runSpeed * Time.fixedDeltaTime;

        if (runVelocity != 0)
        {
            if (runVelocity > 0)
            {
                OnBtnRunRight();
            }
            else
            {
                OnBtnRunLeft();
            }

            transform.position += new Vector3(runVelocity, 0, 0);

            if (isOnFloor)
            {
                anim.SetBool("IsRun", true);
                anim.SetBool("IsJump", false);
            }
        }
        else
        {
            anim.SetBool("IsRun", false);
        }
    }

    void UpdateJump()
    {
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !isBtnJumpPressed)
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
    }

    void UpdateCheckFloor()
    {
        isOnFloor = Physics2D.OverlapCircle((Vector2) transform.position + bottomOffset, collisionRadius, groundLayer);

        if (isOnFloor)
        {
            if (anim.GetBool("IsRun"))
                SetFooTSoundOn(true);
            else
                SetFooTSoundOn(false);
        }
        else
            SetFooTSoundOn(false);
    }

    #endregion

    public void SetDisableInput(bool value)
    {
        isDisableInput = value;
    }

    void Jump()
    {
        rb.velocity = Vector2.zero;
        rb.velocity += Vector2.up * jumpVelocity;

        if (PhotonNetwork.OfflineMode)
        {
            RpcOnJumping();
        }
        else
        {
            photonView.RPC("RpcOnJumping", RpcTarget.All);
        }

    }

    void SetFooTSoundOn(bool isOn)
    {
        if (isOn)
        {
            footSoundSource.UnPause();
        }
        else
        {
            footSoundSource.Pause();
        }
    }

    IEnumerator CancelIgnoreCol(Collider2D thisCollider, Collider2D target)
    {
        yield return new WaitForSecondsRealtime(0.5f);
        Physics2D.IgnoreCollision(thisCollider, target, false);
    }

    #region UI Reference

    public void OnBtnRunLeft()
    {
        transform.rotation = Quaternion.Euler(0, 179, 0);

        if (CameraFollow2D.Instance.IsInstanceExit)
        {
            CameraFollow2D.Instance.ConvertX(true);
        }

        playerItem.IsSwapFace(true);
    }

    public void OnBtnRunRight()
    {
        transform.rotation = Quaternion.Euler(0, 1, 0);

        if (CameraFollow2D.Instance.IsInstanceExit)
        {
            CameraFollow2D.Instance.ConvertX(false);
        }

        playerItem.IsSwapFace(false);
    }

    public void OnBtnJump()
    {
        if (isOnFloor)
        {
            Jump();

            isOnFloor = false;
        }

        isBtnJumpPressed = true;
    }

    public void OnBtnJumpRelese()
    {
        isBtnJumpPressed = false;
    }

    public void OnBtnDropDown()
    {
        Collider2D[] onPlatform = Physics2D.OverlapCircleAll((Vector2) transform.position + bottomOffset, collisionRadius * 2, platformLayer);

        if(onPlatform.Length > 0)
        {
            Collider2D thisCol = GetComponent<Collider2D>();
            
            foreach(Collider2D col in onPlatform)
            {
                Physics2D.IgnoreCollision(thisCol, col);
                StartCoroutine(CancelIgnoreCol(thisCol, col));
            }
        }
    }

    #endregion

    #region  PunRPC

    [PunRPC]
    void RpcOnJumping()
    {
        personalSoundSource.PlayOneShot(jumpSound.audioClip, jumpSound.audioVolume);
        jumpEffect.Play();
    }

    #endregion

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawWireSphere((Vector2) transform.position + bottomOffset, collisionRadius);
    }
}