﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;

public class PlayerBase : MonoBehaviourPun
{
    [SerializeField] GameObject playerLocalControllerUi = null;
    [SerializeField] GameObject playerWorldUI = null;
    [SerializeField] GameObject playerNetworkWorldUI = null;
    [SerializeField] SpriteRenderer[] spriteRenderer = null;
    [SerializeField] TextMeshProUGUI networkNameText = null;

    private AudioClip currentAudioClip;
    private float currentVolume;

    private void Start()
    {
        GetComponent<PlayerHealth>().EventOnPlayerDie += OnPlayerDie;

        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Player"));
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Zombie"));
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("Gadget"));

        SetPlayerLocalControllerUi(photonView.IsMine);
        SetPlayerWorldUi(photonView.IsMine);

        SetPlayerObjName();

        if (PhotonNetwork.OfflineMode)
        {
            return;
        }

        if (!photonView.IsMine)
        {
            foreach (SpriteRenderer sr in spriteRenderer)
            {
                sr.sortingLayerName = "MyEnemy";
            }
        }

        networkNameText.text = photonView.Owner.NickName;
    }

    private void OnEnable()
    {
        GameplayManager.Instance.OnGameEnd += OnGameEnd;
    }

    private void OnDisable()
    {
        GameplayManager.Instance.OnGameEnd -= OnGameEnd;
    }

    private void SetPlayerLocalControllerUi(bool isActive)
    {
        playerLocalControllerUi.SetActive(isActive);
        playerLocalControllerUi.transform.SetParent(null);
    }

    private void SetPlayerWorldUi(bool isMine)
    {
        playerWorldUI.SetActive(isMine);
        playerNetworkWorldUI.SetActive(!isMine);
    }

    private void OnPlayerDie(string killerName)
    {
        if (playerLocalControllerUi != null)
            Destroy(playerLocalControllerUi);
    }

    private void OnGameEnd()
    {
        if (playerLocalControllerUi != null)
            Destroy(playerLocalControllerUi);

        gameObject.SetActive(false);
    }

    void SetPlayerObjName()
    {
        if (PhotonNetwork.OfflineMode) return;

        gameObject.name = gameObject.name + photonView.Owner.NickName;
    }
}