﻿using System.Collections.Generic;
using Facebook.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FacebookMenuManager : MonoBehaviour
{
    [Header("Facebook Ui")]
    [SerializeField] public GameObject facebookLogo = null;
    [SerializeField] public Sprite defaultProfile = null;
    [SerializeField] public Image profileImage = null;
    [SerializeField] public TextMeshProUGUI usernameText = null;
    [SerializeField] public TextMeshProUGUI reputationText = null;
    [SerializeField] public TextMeshProUGUI playCountsText = null;

    [Header("Button Obj")]
    [SerializeField] public GameObject btnOnLoggedIn;
    [SerializeField] public GameObject btnOnGuess;

    public static FacebookMenuManager Instance;

    private void Awake()
    {
        Instance = this;

    }

    private void Start()
    {
        if (FB.IsInitialized && FB.IsLoggedIn)
        {
            SetFacebookProfile(FacebookDataHandler.Instance.ProfilePic, FacebookDataHandler.Instance.FirstName);

            UserData user = FireBaseDatabaseManager.Instance.currentUser;
            SetUserData(user.playTimeAmount, user.userRepAmount);

            ChangeBtnOnLoggedIn(true);
        }
        else
        {
            SetUserData(PlayerPrefs.GetInt(UserPrefsProperty.PlayerPlayTimes, 0), PlayerPrefs.GetInt(UserPrefsProperty.PlayerRep, 0));
        }
    }

    public void SetFacebookProfile(Sprite profileSprite, string usernameString)
    {
        if (profileSprite != null)
            profileImage.sprite = profileSprite;

        if (usernameString != "")
            usernameText.text = $"Hi, {usernameString}";

        if (usernameString == "Guess")
            facebookLogo.SetActive(false);
        else
            facebookLogo.SetActive(true);
    }

    public void ChangeBtnOnLoggedIn(bool value)
    {
        if (value)
        {
            btnOnLoggedIn.SetActive(true);
            btnOnGuess.SetActive(false);
        }
        else
        {
            btnOnLoggedIn.SetActive(false);
            btnOnGuess.SetActive(true);
        }
    }

    public void SetUserData(int playCounts, int currentReputation)
    {
        playCountsText.text = $"Play times : {playCounts}";
        reputationText.text = $"REP : {currentReputation}";
    }

    #region UI

    public void OnBtnFacebookLogOut()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        FacebookDataHandler.Instance.FBLogOut();

        MenuManager.Instance.mainMenuUi.SetActive(false);
        MenuManager.Instance.facebookUi.SetActive(true);

        SetFacebookProfile(defaultProfile, "Guess");
        ChangeBtnOnLoggedIn(false);
    }

    public void OnBtnFacebookLogin()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        FacebookDataHandler.Instance.FBLogin();
    }

    public void OnBtnGuessLogin()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        FacebookMenuManager.Instance.SetFacebookProfile(defaultProfile, "Guess");

        MenuManager.Instance.nameConfigUi.SetActive(true);
        MenuManager.Instance.facebookUi.SetActive(false);

        if (FacebookMenuManager.Instance != null)
        {
            SetUserData(PlayerPrefs.GetInt(UserPrefsProperty.PlayerPlayTimes, 0), PlayerPrefs.GetInt(UserPrefsProperty.PlayerRep, 0));
        }

        FireBaseDatabaseManager.Instance.connectWithDatabase = false;

        ChangeBtnOnLoggedIn(false);
    }

    public void FBShareLink()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        FB.ShareLink(new System.Uri("https://www.youtube.com/channel/UC7sCqWkewe6VbrWgCgsls5A?view_as=subscriber"), "Check my Chanel!",
            "A lot of good programming tutorials let's join me!",
            new System.Uri("https://yt3.ggpht.com/a-/AAuE7mDWGZWyo-ItmXIB38I1eSZ50w95XKxS8s2XGCYI=s88-c-k-c0xffffffff-no-rj-mo"));
    }

    public void FBGameRequest()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        FB.AppRequest("Hi! Come and play this awesome game!", title: "Apocalypse Arena");
    }

    #endregion
}