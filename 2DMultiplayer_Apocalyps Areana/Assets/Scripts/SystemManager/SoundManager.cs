﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    [SerializeField] GameAudioInfo btnSound = null;
    public GameAudioInfo GetBtnSound
    {
        get { return btnSound; }
    }

    [SerializeField] GameAudioInfo typeSound = null;

    static public SoundManager Instance;

    private AudioSource audioSource;

    public bool IsExit { get { return Instance != null; } }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        audioSource = GetComponent<AudioSource>();
    }

    public void PlayBtnSound()
    {
        audioSource.PlayOneShot(btnSound.audioClip, btnSound.audioVolume);
    }

    public void PlayTypingSound()
    {
        audioSource.PlayOneShot(typeSound.audioClip, typeSound.audioVolume);
    }

    public void PlaySoundEffect(AudioClip audioClip, float volume = 1f)
    {
        audioSource.PlayOneShot(audioClip, volume);
    }

}