﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PopupManager : MonoBehaviourPunCallbacks
{
    [SerializeField]
    GameObject popupPanel = default;

    [SerializeField]
    Button okButton = default;

    [SerializeField]
    TextMeshProUGUI popupText = default;

    public static PopupManager Instance;

    private Action okButtonCallback;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        okButton.onClick.AddListener(() =>
        {
            popupPanel.gameObject.SetActive(false);
            okButtonCallback?.Invoke();
            okButtonCallback = null;
        });

        popupPanel.gameObject.SetActive(false);
    }

    public void Open(string message, Action callback)
    {
        popupPanel.gameObject.SetActive(true);
        popupText.text = message;
        okButtonCallback = callback;
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Open("Can't connect to server.\nPlease check your network \nand try again. ",() => SceneManager.LoadScene(0));
    }
}