﻿using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FacebookDataHandler : MonoBehaviour
{
    public string FirstName;
    public Sprite ProfilePic;
    public Text FriendsText;

    public static FacebookDataHandler Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        FB.Init(SetInit, OnHideUnity);
    }

    void SetInit()
    {
        if (FB.IsInitialized)
        {
            FB.ActivateApp();
            Debug.Log("FB is initialized");
        }
        else
        {
            Debug.Log("Failed to initialized facebook SDK");
            PopupManager.Instance.Open("Can't connect to facebook SDK.\nGame will be restart.", () => SceneManager.LoadScene(0));
            return;
        }

        if (FB.IsLoggedIn)
        {
            LoadPlayerFacebookData();
        }
    }

    void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void FBLogin()
    {
        List<string> permissions = new List<string>(); //what info that we want to access from user
        permissions.Add("public_profile");
        permissions.Add("email");

        FB.LogInWithReadPermissions(permissions, AuthCallBack);
    }

    public void FBLogOut()
    {
        FB.LogOut();
    }

    void AuthCallBack(IResult result)
    {
        if (FB.IsLoggedIn)
        {
            LoadPlayerFacebookData();

            if (!MenuManager.Instance.mainMenuUi.activeInHierarchy)
            {
                MenuManager.Instance.nameConfigUi.SetActive(true);
                MenuManager.Instance.facebookUi.SetActive(false);
            }
            else
            {
                MenuManager.Instance.mainMenuUi.SetActive(false);
                MenuManager.Instance.mainMenuUi.SetActive(true);
            }

            Debug.Log("FB is logged in");
        }
        else
        {
            Debug.Log("FB is not logged in");
        }

        FacebookMenuManager.Instance.ChangeBtnOnLoggedIn(FB.IsLoggedIn);

    }

    void LoadPlayerFacebookData()
    {
        FB.API("/me?fields=first_name", HttpMethod.GET, GetUsername);
        FB.API("/me/picture?type=square&height=128&width=128", HttpMethod.GET, DisplayProfilePic);
        //FB.API ("me/friends", HttpMethod.GET, FriendCallBack);

        FireBaseDatabaseManager.Instance.ConnectToDatabase(AccessToken.CurrentAccessToken.UserId);
        FacebookMenuManager.Instance.ChangeBtnOnLoggedIn(FB.IsLoggedIn);
        PopupManager.Instance.Open("Facebook login success.", null);
    }

    void GetUsername(IResult result)
    {
        if (result.Error == null)
        {
            FirstName = (string) result.ResultDictionary["first_name"];

        }
        else
        {
            FirstName = "Facebook Guess";
            Debug.Log(result.Error);
        }

        FacebookMenuManager.Instance.SetFacebookProfile(null, FirstName);
    }

    void DisplayProfilePic(IGraphResult result)
    {
        if (result.Texture != null)
        {
            ProfilePic = Sprite.Create(result.Texture, new Rect(0, 0, 128, 128), new Vector2());
            FacebookMenuManager.Instance.SetFacebookProfile(ProfilePic, "");
        }
    }

    void FriendCallBack(IGraphResult result)
    {
        IDictionary<string, object> data = result.ResultDictionary;
        List<object> friends = (List<object>) data["data"];

        foreach (object obj in friends)
        {
            FriendsText.text += ((Dictionary<string, object>) obj) ["name"];
        }
    }

}