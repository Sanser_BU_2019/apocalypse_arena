﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.UI;

public class AdmobManager : MonoBehaviour
{
    #region  singleton

    public static AdmobManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    #endregion

    private BannerView gameBanner;
    private const string menuBannerID = "ca-app-pub-3940256099942544/6300978111";

    public enum FullscreenAdSelected
    {
        EndMatchFullscreenAd,
        nothing
    }
    private InterstitialAd currentFullscreenAd;
    private string currentFullscreenAdId;
    private InterstitialAd endMatchFullscreenAd;
    private const string endMatchFullscreenAdId = "ca-app-pub-3940256099942544/1033173712";

    public enum RewardedAdSelected
    {
        EndMatchAddScoreRewarded,
        nothing
    }
    private RewardedAd endMatchAddScoreRewarded;
    private const string endMatchAddScoreRewardedID = "ca-app-pub-3940256099942544/5224354917";

    private void Start()
    {
        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(initStatus => { });

        RequestRewardedAd(RewardedAdSelected.EndMatchAddScoreRewarded);
        RequestFullscreenAd(FullscreenAdSelected.EndMatchFullscreenAd);
    }

    #region  BannerViewAds

    public void RequestBannerView(bool showBannerImmediately = false)
    {
        BannerView newBanner = new BannerView(menuBannerID, AdSize.Banner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();

        //newBanner.LoadAd(request);

        if (showBannerImmediately)
        {
            //newBanner.Show();
        }

        gameBanner = newBanner;
    }

    public void ShowBanner()
    {
        //gameBanner.Show();
    }

    public void HideBanner()
    {
        //gameBanner.Hide();
    }

    #endregion

    #region FullScreenAds

    private void RequestFullscreenAd(FullscreenAdSelected selected)
    {
        string targetAdId = "";

        switch (selected)
        {
            case FullscreenAdSelected.EndMatchFullscreenAd:
                targetAdId = endMatchFullscreenAdId;
                break;
        }

        InterstitialAd fullscreenAd = new InterstitialAd(targetAdId);

        switch (selected)
        {
            case FullscreenAdSelected.EndMatchFullscreenAd:
                endMatchFullscreenAd = fullscreenAd;
                break;
        }

        AdRequest request = new AdRequest.Builder().Build();

        fullscreenAd.LoadAd(request);

    }

    public void ShowFullscreenAd(FullscreenAdSelected selected)
    {
        switch (selected)
        {
            case FullscreenAdSelected.EndMatchFullscreenAd:
                VisualizeFullscreenAd(endMatchFullscreenAd);
                break;
        }

        RequestFullscreenAd(selected);
    }

    private void VisualizeFullscreenAd(InterstitialAd targetFullscreenAd)
    {
        if (targetFullscreenAd.IsLoaded())
            targetFullscreenAd.Show();
    }

    #endregion

    #region RewardedAds

    private void RequestRewardedAd(RewardedAdSelected selected)
    {
        string targetAdId = "";

        switch (selected)
        {
            case RewardedAdSelected.EndMatchAddScoreRewarded:
                targetAdId = endMatchAddScoreRewardedID;
                break;
        }

        RewardedAd rewardedAd = new RewardedAd(targetAdId);

        switch (selected)
        {
            case RewardedAdSelected.EndMatchAddScoreRewarded:
                endMatchAddScoreRewarded = rewardedAd;
                break;
        }

        AdRequest request = new AdRequest.Builder().Build();

        rewardedAd.LoadAd(request);
    }

    public void ShowRewardedAd(RewardedAdSelected selected, EventHandler<Reward> CallBackRewarded)
    {
        switch (selected)
        {
            case RewardedAdSelected.EndMatchAddScoreRewarded:
                VisualizeRewardedAd(endMatchAddScoreRewarded, CallBackRewarded);
                break;
        }

        RequestRewardedAd(selected);
    }

    private void VisualizeRewardedAd(RewardedAd targetRewarded, EventHandler<Reward> CallBackRewarded)
    {
        if (CallBackRewarded != null)
            targetRewarded.OnUserEarnedReward += CallBackRewarded;

        if (targetRewarded.IsLoaded())
            targetRewarded.Show();
    }

    public void TestReward()
    {
        ShowRewardedAd(RewardedAdSelected.EndMatchAddScoreRewarded, null);
    }

    #endregion
}