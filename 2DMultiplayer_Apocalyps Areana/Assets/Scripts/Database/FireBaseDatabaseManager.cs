﻿using System.Collections;
using System.Collections.Generic;
using Proyecto26;
using UnityEditor;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.SceneManagement;

public class FireBaseDatabaseManager : MonoBehaviour
{
    public bool connectWithDatabase;
    string currentUserId;
    public UserData currentUser;

    public static FireBaseDatabaseManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ConnectToDatabase(string facebookId)
    {
        if (facebookId == null || facebookId == "" || facebookId == " ")
            return;

        currentUserId = facebookId;
        connectWithDatabase = true;

        InitializeDatabase();
    }

    private void InitializeDatabase()
    {
        currentUser = null;
        RestClient.Get<UserData>("https://apocalypse-arena.firebaseio.com/" + currentUserId + ".json").Then(response =>
        {
            currentUser = response;
            UpdateCurrentUserData();

        }).Catch(e => {
            PopupManager.Instance.Open("Can't connect to database.\nGame will be restart.", () => SceneManager.LoadScene(0));
        }) .Finally(Check);
    }

    void Check()
    {
        if (currentUser == null)
        {
            RegisNewUserToDatabase();
        }
    }

    void RegisNewUserToDatabase()
    {
        UserData newUser = new UserData(currentUserId, 0, 0);
        RestClient.Put("https://apocalypse-arena.firebaseio.com/" + currentUserId + ".json", newUser);

        currentUser = newUser;
    }

    void UpdateCurrentUserData()
    {
        RestClient.Put("https://apocalypse-arena.firebaseio.com/" + currentUserId + ".json", currentUser);

        if (FacebookMenuManager.Instance != null)
        {
            FacebookMenuManager.Instance.SetUserData(currentUser.playTimeAmount, currentUser.userRepAmount);
        }
    }

    public void AddReputation(int value)
    {
        currentUser.userRepAmount += value;
        UpdateCurrentUserData();
    }

    public void SetReputation(int value)
    {
        currentUser.userRepAmount = value;
        UpdateCurrentUserData();
    }

    public void AddUserPlayTime()
    {
        currentUser.playTimeAmount++;
        UpdateCurrentUserData();
    }
}