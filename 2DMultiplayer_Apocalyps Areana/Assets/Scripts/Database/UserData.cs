﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UserData
{
   public string userFacebookId;
   public int userRepAmount;
   public int playTimeAmount;

   public UserData(string userFacebookId, int userRepAmount, int playTimeAmount)
   {
       this.userFacebookId = userFacebookId;
       this.userRepAmount = userRepAmount;
       this.playTimeAmount = playTimeAmount;
   }
}
