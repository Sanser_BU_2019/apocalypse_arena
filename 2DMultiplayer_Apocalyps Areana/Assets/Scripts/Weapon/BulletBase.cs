﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class BulletBase : MonoBehaviourPun
{
    [SerializeField] Rigidbody2D rb2D = null;
    [SerializeField] float damage = 10;
    [SerializeField] float speed = 10;
    [SerializeField] float lifeTime = 10;

    string ownerGameObjectName;
    string bulletOwnerName;
    bool isWasHitSomething;

    void Start()
    {
        if (photonView.IsMine)
        {
            StartCoroutine(DestroyBullet());
            var ping = PhotonNetwork.GetPing();
            photonView.RPC("SyncNetworkPositionLag", RpcTarget.Others, ping);
            isWasHitSomething = false;
        }

        if (PhotonNetwork.OfflineMode) return;

        bulletOwnerName = photonView.Owner.NickName;
    }

    public void SetBulletMovement(Vector3 direction, GameObject ownerObj)
    {
        if (PhotonNetwork.OfflineMode)
        {
            RPCSetBulletMovement(direction, ownerObj.name);
        }
        else
        {
            photonView.RPC("RPCSetBulletMovement", RpcTarget.All, direction, ownerObj.name);
        }
    }

    void SetBulletOwner(GameObject ownerObj)
    {
        ownerGameObjectName = ownerObj.name;
    }

    IEnumerator DestroyBullet()
    {
        yield return new WaitForSecondsRealtime(lifeTime);

        if (PhotonNetwork.OfflineMode)
        {
            RPCDestroyBullet();
        }
        else
            photonView.RPC("RPCDestroyBullet", RpcTarget.All);
    }

    #region PunRPC

    [PunRPC]
    void SyncNetworkPositionLag(int remotePing)
    {
        var ping = PhotonNetwork.GetPing();
        var delay = (float) (ping / 2 + remotePing / 2);
        transform.position += (Vector3) (rb2D.velocity * (delay / 1000));
    }

    [PunRPC]
    void RPCSetBulletMovement(Vector3 direction, string ownerObjName)
    {
        rb2D.velocity = direction * speed;

        ownerGameObjectName = ownerObjName;

    }

    [PunRPC]
    void RPCDestroyBullet()
    {
        Destroy(gameObject);

    }

    #endregion

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!photonView.IsMine || other.CompareTag("Bullet") || other.CompareTag("IgnoreBullet") || other.CompareTag("Platform") ||
            other.CompareTag("Item") || other.gameObject.CompareTag("SpawnPoint") || other.gameObject.CompareTag("Object"))
        {
            return;
        }

        PhotonView targetView = other.GetComponent<PhotonView>();

        if (targetView != null && !isWasHitSomething)
        {
            if (!targetView.IsMine || targetView.IsSceneView)
            {
                IDamageAble target = targetView.GetComponent<IDamageAble>();

                if (target != null && !target.CheckIsDied())
                {
                    target.TakeDamage(damage, bulletOwnerName, ownerGameObjectName, PhotonNetwork.LocalPlayer);

                    if (PhotonNetwork.OfflineMode)
                    {
                        RPCDestroyBullet();
                    }
                    else
                    {
                        photonView.RPC("RPCDestroyBullet", RpcTarget.All);
                    }
                    isWasHitSomething = true;
                }
                else
                {
                    return;
                }
            }
        }

        if (PhotonNetwork.OfflineMode)
        {
            RPCDestroyBullet();
        }
        else
        {
            photonView.RPC("RPCDestroyBullet", RpcTarget.All);
        }

    }
}