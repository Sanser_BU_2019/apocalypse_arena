﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WeaponBase : MonoBehaviourPun
{
    public enum WeaponReloadMode
    {
        Magazine,
        OneByOne
    }

    [SerializeField] Sprite weaponSprite = null;
    [SerializeField] WeaponReloadMode reloadMode = WeaponReloadMode.Magazine;
    [SerializeField] float reloadTime = 1;
    [SerializeField] int maxAmmo = 8;
    [SerializeField][Range(0, 5)] float fireRate = 0.5f;
    [SerializeField] GameObject bulletPrefab = null;
    [SerializeField] ParticleSystem muzzleFlash = null;
    [SerializeField] Transform[] shootSpawnPoint = null;

    [Header("Accuracy")]
    [SerializeField] float spreadRate = 1;

    [Header("Recoil")]
    [SerializeField] Transform recoilPos = null;
    [SerializeField][Range(0, 1)] float recoilKickUpPossible = 0.5f;
    [SerializeField] Vector2 recoilKickUpMinMax = Vector2.zero;
    [SerializeField] Vector2 recoilKickDownMinMax = Vector2.zero;
    [SerializeField] float recoilRecoverRate = 1f;

    [Header("Sound")]
    [SerializeField] GameAudioInfo shootAudio = null;
    [SerializeField] GameAudioInfo reloadAudio = null;
    [SerializeField] GameAudioInfo emptyBulletAudio = null;
    [SerializeField] GameAudioInfo rackAudio = null; //for gun that have rack sound

    protected AudioSource weaponAudioSource;
    protected PlayerHodingWeapon playerCarrier;
    protected PlayerShoot playerGunController;

    protected float currentRecoilZPos;

    protected bool isReloadingWeapon;
    protected float reloadCountTime;
    protected bool isFireRateFit;
    protected float fireRateCountTime;
    private int currentAmmo;

    public Sprite GetWeaponSprite
    {
        get { return weaponSprite; }
    }

    public int GetMaxAmmo
    {
        get { return maxAmmo; }
    }

    public int GetCurrentAmmo
    {
        get { return currentAmmo; }
    }

    public bool IsReloading
    {
        get { return isReloadingWeapon; }
    }

    public bool IsFireRateFit
    {
        get { return isFireRateFit; }
    }

    private void Awake()
    {
        weaponAudioSource = GetComponent<AudioSource>();

        PhotonNetwork.AllocateViewID(photonView);

        currentAmmo = GetMaxAmmo;
        fireRateCountTime = 0;

        isReloadingWeapon = false;
        isFireRateFit = true;
    }

    private void Update()
    {
        if (!photonView.IsMine) return;

        if (isReloadingWeapon)
        {
            reloadCountTime -= Time.deltaTime;

            if (reloadCountTime <= 0)
            {
                isReloadingWeapon = false;

                switch (reloadMode)
                {
                    case WeaponReloadMode.Magazine:
                        loadNewMagazine();
                        break;

                    case WeaponReloadMode.OneByOne:
                        loadNewBullet();
                        break;
                }
            }
        }

        if (!isFireRateFit)
        {
            fireRateCountTime -= Time.deltaTime;

            if (fireRateCountTime <= 0)
            {

                isFireRateFit = true;
            }
        }

        if (recoilPos != null)
        {
            currentRecoilZPos = Mathf.Lerp(currentRecoilZPos, 0, recoilRecoverRate * Time.deltaTime);
            recoilPos.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, currentRecoilZPos);
        }
    }

    public bool ShootBullet()
    {
        if (currentAmmo <= 0 || fireRateCountTime > 0)
        {
            return false;
        }

        CancelReload();

        if (recoilPos != null)
        {
            ApplyRecoil();
        }

        currentAmmo--;

        if (PhotonNetwork.OfflineMode)
        {
            playerGunController.RpcWeaponOnShoot();
        }
        else
        {
            playerGunController.photonView.RPC("RpcWeaponOnShoot", RpcTarget.All);
        }

        isFireRateFit = false;
        fireRateCountTime = fireRate;

        foreach (Transform t in shootSpawnPoint)
        {
            float targetRestlessZ = Random.Range(-spreadRate, spreadRate);
            Quaternion originRotation = t.localRotation;

            t.localRotation = Quaternion.Euler(t.rotation.x, t.rotation.y, t.eulerAngles.z + targetRestlessZ);

            GameObject bullet = PhotonNetwork.Instantiate(bulletPrefab.name, t.position, t.rotation, 0);
            bullet.GetComponent<BulletBase>().SetBulletMovement(t.transform.right, playerGunController.gameObject);

            t.localRotation = originRotation;
        }

        return true;
    }

    void ApplyRecoil()
    {
        float randRecoilDirect = Random.Range(0f, 1f);

        if (randRecoilDirect < recoilKickUpPossible)
        {
            currentRecoilZPos += Random.Range(recoilKickUpMinMax.x, recoilKickUpMinMax.y);
        }
        else
        {
            currentRecoilZPos -= Random.Range(recoilKickDownMinMax.x, recoilKickDownMinMax.y);
        }
    }

    void loadNewMagazine()
    {
        currentAmmo = maxAmmo;
        playerCarrier.UpdateUI();

    }

    void loadNewBullet()
    {
        currentAmmo++;
        playerCarrier.UpdateUI();

        if (currentAmmo < maxAmmo)
        {
            Reload();
        }
    }

    public bool Reload()
    {
        if (isReloadingWeapon || !isFireRateFit || currentAmmo == maxAmmo) return false;

        isReloadingWeapon = true;
        reloadCountTime = reloadTime;

        if (PhotonNetwork.OfflineMode)
        {
            playerGunController.RpcPlayReloadSound();
        }
        else
        {
            playerGunController.photonView.RPC("RpcPlayReloadSound", RpcTarget.All);
        }

        return isReloadingWeapon;
    }

    public void CancelReload()
    {
        isReloadingWeapon = false;
    }

    public void SetHoldingSlot(PlayerHodingWeapon owner, PlayerShoot playerShooter)
    {
        playerCarrier = owner;
        playerGunController = playerShooter;
    }

    public void UpdateReloadUi(Image targetImage, TextMeshProUGUI targetText)
    {
        targetImage.fillAmount = (reloadTime - reloadCountTime) / reloadTime;
        targetText.text = reloadCountTime.ToString("F1") + " s.";
    }

    public void UpdateFirerateImage(Image targetImage)
    {
        targetImage.fillAmount = fireRateCountTime / fireRate;
    }

    public void DestroyWeapon()
    {
        Destroy(gameObject);
    }

    #region Call on Player's RPC

    public void OnWeaponShoot()
    {
        weaponAudioSource.PlayOneShot(shootAudio.audioClip, shootAudio.audioVolume);

        if (muzzleFlash != null)
        {
            muzzleFlash.Play();
        }

        Invoke("PlayRackSound", 0.25f);
    }

    void PlayRackSound()
    {
        if (rackAudio.audioClip != null)
            weaponAudioSource.PlayOneShot(rackAudio.audioClip, rackAudio.audioVolume);
    }

    public void PlayEmptyBulletSound()
    {
        if (emptyBulletAudio != null && weaponAudioSource != null)
            weaponAudioSource.PlayOneShot(emptyBulletAudio.audioClip, reloadAudio.audioVolume);
    }

    public void PlayReloadSound()
    {
        if (reloadAudio != null && weaponAudioSource != null)
            weaponAudioSource.PlayOneShot(reloadAudio.audioClip, reloadAudio.audioVolume);
    }
    #endregion

}