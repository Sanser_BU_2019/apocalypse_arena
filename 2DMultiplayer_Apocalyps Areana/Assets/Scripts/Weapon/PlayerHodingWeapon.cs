﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class PlayerHodingWeapon
{
    public WeaponBase weapon = null;
    public GameObject holdingObj = null;
    public Image weaponImage = null;
    public TextMeshProUGUI textAmmoCount = null;
    public Transform arrowImage = null;

    public void ChangeWeapon(WeaponBase newWeapon)
    {
        weapon = newWeapon;
        UpdateUI();
    }

    public void UpdateUI()
    {
        if (weapon != null)
        {
            if (weaponImage != null)
            {
                weaponImage.enabled = true;
                weaponImage.sprite = weapon.GetWeaponSprite;
            }

            textAmmoCount.text = string.Format("{0}/{1}", weapon.GetCurrentAmmo, weapon.GetMaxAmmo);
        }
        else
        {
            weaponImage.enabled = false;
            textAmmoCount.text = "";;
        }

    }

    public void SetHoldingWeaponActive(bool value)
    {
        if (holdingObj != null)
            holdingObj.SetActive(value);

        if (arrowImage != null)
            arrowImage.gameObject.SetActive(value);
    }
}