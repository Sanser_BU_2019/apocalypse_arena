﻿using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Analytics;

using Hashtable = ExitGames.Client.Photon.Hashtable;

public class GameScoreManager : MonoBehaviourPunCallbacks
{
    [Header("All Ui")]
    [SerializeField] GameObject scoreUi = null;
    [SerializeField] public GameObject endGameUi = null;

    [Header("Score Container")]
    [SerializeField] GameObject scoreReportPrefab = null;
    [SerializeField] Transform playerScoreContainer = null;

    [Header("Game End Score")]
    [SerializeField] GameObject endScoreReportPrefab = null;
    [SerializeField] Transform scoreEndContainer = null;

    public delegate void OnScoreWasChange();
    public OnScoreWasChange PlayerScoreChange;

    Hashtable initialProps;

    Dictionary<int, PlayerScoreReport> allPlayerScoreReport = new Dictionary<int, PlayerScoreReport>();
    List<PlayerScoreReport> allScoreReportList = new List<PlayerScoreReport>();

    PlayerScoreReport localPlayerScoreReport;

    private int currentLocalPlayerScore;
    public int GetCurrentLocalPlayerScore { get { return currentLocalPlayerScore; } }

    public static GameScoreManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        GameplayManager.Instance.OnGameEnd += ReportGameEndScore;

        scoreUi.SetActive(true);
        endGameUi.SetActive(false);

        initialProps = new Hashtable();
        initialProps.Add(GameProperty.PLAYER_SCORE, 0);

        PhotonNetwork.LocalPlayer.SetCustomProperties(initialProps);

        for (int i = 0; i < playerScoreContainer.childCount; i++)
        {
            Destroy(playerScoreContainer.GetChild(i).gameObject);
        }

        CreateAllPlayerScoreReport();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
            AddPlayerScore(100);
    }

    void CreateAllPlayerScoreReport()
    {
        foreach (Player p in PhotonNetwork.PlayerList)
        {
            GameObject newPlayerReportPrefab = Instantiate(scoreReportPrefab);
            newPlayerReportPrefab.transform.SetParent(playerScoreContainer);
            newPlayerReportPrefab.name = "PlayerScoreReport " + p.NickName;

            PlayerScoreReport newPlayerReport = newPlayerReportPrefab.GetComponent<PlayerScoreReport>();
            allPlayerScoreReport.Add(p.ActorNumber, newPlayerReport);
            allScoreReportList.Add(newPlayerReport);

            newPlayerReport.SetReportScore(0);
            newPlayerReport.SetReportName("Loading...", "");

            if (p == PhotonNetwork.LocalPlayer)
            {
                newPlayerReport.SetReportLocalOwner();
                localPlayerScoreReport = newPlayerReport;
            }

            object playerScore;
            if (p.CustomProperties.TryGetValue(GameProperty.PLAYER_SCORE, out playerScore))
            {
                newPlayerReport.SetReportName("allPlayerScoreReport.Count.ToString()", p.NickName);
            }
            else
            {
                newPlayerReport.SetReportName("allPlayerScoreReport.Count.ToString()", p.NickName);
            }
        }
    }

    public void AddPlayerScore(int value)
    {
        currentLocalPlayerScore += value;

        if (localPlayerScoreReport != null)
        {
            localPlayerScoreReport.SetReportScore(currentLocalPlayerScore);
        }

        SortingScoreRank();

        initialProps[GameProperty.PLAYER_SCORE] = currentLocalPlayerScore;
        PhotonNetwork.LocalPlayer.SetCustomProperties(initialProps);
    }

    public override void OnPlayerPropertiesUpdate(Player target, Hashtable changedProps)
    {
        object newData;
        if (changedProps.TryGetValue(GameProperty.PLAYER_SCORE, out newData))
        {
            allPlayerScoreReport[target.ActorNumber].SetReportScore((int) newData);

            SortingScoreRank();
        }
    }

    void SortingScoreRank()
    {
        for (int i = 0; i < allScoreReportList.Count; i++)
        {
            for (int j = i + 1; j < allScoreReportList.Count; j++)
            {
                if (allScoreReportList[j].currentReportScore > allScoreReportList[i].currentReportScore)
                {
                    PlayerScoreReport tmp = allScoreReportList[i];
                    allScoreReportList[i] = allScoreReportList[j];
                    allScoreReportList[j] = tmp;
                }
            }
        }

        for (int i = 0; i < allScoreReportList.Count; i++)
        {
            allScoreReportList[i].gameObject.transform.SetSiblingIndex(i);
            allScoreReportList[i].SetReportName((i + 1).ToString(), "");
        }

    }

    void ReportGameEndScore()
    {
        List<PlayerScoreReportEndGame> reportList = new List<PlayerScoreReportEndGame>();

        scoreUi.SetActive(false);
        endGameUi.SetActive(true);

        for (int i = 0; i < allScoreReportList.Count; i++)
        {
            GameObject reportPrefab = Instantiate(endScoreReportPrefab);
            reportPrefab.transform.SetParent(scoreEndContainer, false);

            PlayerScoreReportEndGame report = reportPrefab.GetComponent<PlayerScoreReportEndGame>();

            if (report != null)
            {
                reportList.Add(report);
                report.SetFinalScoreReport(allScoreReportList[i].ownerName, i + 1, allScoreReportList[i].currentReportScore);
            }
        }

        if (FireBaseDatabaseManager.Instance.connectWithDatabase)
            FireBaseDatabaseManager.Instance.AddReputation(currentLocalPlayerScore);
        else
        {
            PlayerPrefs.SetInt(UserPrefsProperty.PlayerRep, currentLocalPlayerScore + PlayerPrefs.GetInt(UserPrefsProperty.PlayerRep, 0));
        }
    }
}