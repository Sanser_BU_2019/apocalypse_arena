﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class SceneAiManager : MonoBehaviourPun
{
    public static SceneAiManager Instance;
    public bool IsInstanceExit
    {
        get { return Instance != null; }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void SpawnNewEnemy(string objName, Vector3 position, float timeUnitilSpawn)
    {
        objName = objName.Replace("(clone)","").Trim();
        StartCoroutine(SpawnNewEnemyDelay("Zombie/" +objName, position, timeUnitilSpawn));
    }

    public void SpawnNewWeapon(string objName, Vector3 position, float timeUnitilSpawn)
    {
        objName = objName.Replace("(clone)","").Trim();
        StartCoroutine(SpawnNewEnemyDelay("Item/" + objName, position, timeUnitilSpawn));
    }

    public void SpawnNewMedic(Vector3 position, float timeUnitilSpawn)
    {
        StartCoroutine(SpawnNewEnemyDelay("Item/Item_Medic", position, timeUnitilSpawn));
    }

    IEnumerator SpawnNewEnemyDelay(string objName, Vector3 position, float timeUnitilSpawn)
    {
        yield return new WaitForSecondsRealtime(timeUnitilSpawn);

        if (PhotonNetwork.OfflineMode)
        {
            GameObject newObj = Resources.Load(objName) as GameObject;
            Instantiate(newObj, position, Quaternion.identity);
        }
        else
        {
            PhotonNetwork.InstantiateSceneObject(objName, position, Quaternion.identity, 0);
        }

    }
}