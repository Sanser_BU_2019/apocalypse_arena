﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class BombController : MonoBehaviourPun
{
    [SerializeField] float damage = 90;
    [SerializeField] LayerMask damageAblelayer = 0;
    [SerializeField] float throwForce = 500;
    [SerializeField] float explosionDelay = 3;
    [SerializeField] float bombRadius = 3;
    [SerializeField] GameObject bombEffect = null;
    [SerializeField] Rigidbody2D rb2D = null;

    Player playerOwner;
    string charOwnerName;

    bool bombStart = false;

    public void ThrowBomb(Vector3 direction, Player owner, string charName)
    {
        rb2D.AddForce(direction * throwForce);
        rb2D.AddTorque(30);
        playerOwner = owner;
        charOwnerName = charName;

    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (photonView.IsMine)
        {
             StartCoroutine(BeginExplosion());
        }
           
    }

    IEnumerator BeginExplosion()
    {
        if (!bombStart)
        {
            yield return new WaitForSecondsRealtime(explosionDelay);

            if (PhotonNetwork.OfflineMode)
            {
                RPCCreateExplosion();
            }
            else
            {
                photonView.RPC("RPCCreateExplosion", RpcTarget.All);
            }

            bombStart = true;
        }
    }

    #region Pun RPC

    [PunRPC]
    void RPCCreateExplosion()
    {
        if (photonView.IsMine)
        {
            Collider2D[] collision = Physics2D.OverlapCircleAll((Vector2) transform.position, bombRadius, damageAblelayer);

            List<IDamageAble> hitObj = new List<IDamageAble>();

            foreach (Collider2D col in collision)
            {
                hitObj.Add(col.GetComponent<IDamageAble>());
            }

            foreach (IDamageAble hit in hitObj)
            {
                hit.TakeDamage(damage, playerOwner.NickName, charOwnerName, playerOwner);
            }
        }

        Destroy(gameObject);
        Instantiate(bombEffect, transform.position, Quaternion.identity);
    }

    #endregion

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, bombRadius);
    }
}