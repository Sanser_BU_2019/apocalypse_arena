﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.Analytics;

using Hashtable = ExitGames.Client.Photon.Hashtable;

public class GameplayManager : MonoBehaviourPunCallbacks
{
    [SerializeField] bool isOfflineMode = false;
    [SerializeField] GameObject sceneListener = null;
    [SerializeField] double gameTimeUntilEnd = 200;

    [Header("Player Spawner")]
    [SerializeField] GameObject playerPrefab = null;
    [SerializeField] List<SpawnPoint> spawnPoints = null;
    [SerializeField] float minYFallAble = -20;

    [Header("Ui Obj")]
    [SerializeField] GameObject sceneUi = null;
    [SerializeField] GameObject endGameUi = null;
    [SerializeField] GameObject spawnBtn = null;
    [SerializeField] GameObject killFeedUi = null;
    [SerializeField] GameObject killerReport = null;
    [SerializeField] GameObject facebookReport = null;
    [SerializeField] Transform itemSelector = null;

    [Header("Ui")]
    [SerializeField] TextMeshProUGUI gameTimeCountText = null;
    [SerializeField] TextMeshProUGUI pingRateText = null;
    [SerializeField] TextMeshProUGUI fpsRateText = null;

    public delegate void GameWasEnd();
    public event GameWasEnd OnGameEnd;

    private Vector3 currentSpawnPos;
    private GameObject currentPlayer;
    private double currentTimeCount;
    private bool isGameEnd;

    public bool IsGameEnd
    {
        get { return isGameEnd; }

    }

    public GameObject KillFeedUi
    {
        get { return killFeedUi; }

    }

    public Transform ItemSelector
    {
        get { return itemSelector; }

    }

    public static GameplayManager Instance;
    public bool IsInstanceExit
    {
        get { return Instance != null; }
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        Application.targetFrameRate = 60;

#if UNITY_EDITOR
        if (isOfflineMode)
        {
            PhotonNetwork.OfflineMode = true;
        }
#endif
    }

    private const string CHARACTERS_FOLDER_NAME = "Chracters/";

    void Start()
    {
        if (PlayerPrefs.HasKey(PlayerProperty.PLAYER_CHARACTER_NAME) && !PhotonNetwork.OfflineMode) //Setup player prefab
        {
            string playerPrefabLocation = PlayerPrefs.GetString(PlayerProperty.PLAYER_CHARACTER_NAME);
            playerPrefab = Resources.Load<GameObject>(CHARACTERS_FOLDER_NAME + playerPrefabLocation);
        }

        OnBtnSpawn();

        OnGameEnd += OnGameWasEnd;

        currentTimeCount = gameTimeUntilEnd;
        isGameEnd = false;

        StartCoroutine(DisplayPing());
        StartCoroutine(DisplayFPS());

        sceneUi.SetActive(true);
        endGameUi.SetActive(false);
        facebookReport.SetActive(false);

        if (PhotonNetwork.OfflineMode) return;

        PhotonNetwork.AutomaticallySyncScene = false;

        Hashtable props = new Hashtable
        { { GameProperty.ROOM_TIME_COUNT, currentTimeCount }, { GameProperty.IS_GAME_WAS_END, isGameEnd }
        };
        PhotonNetwork.CurrentRoom.SetCustomProperties(props);

        if (AdmobManager.Instance != null)
        {
            AdmobManager.Instance.HideBanner();
        }

        if (PhotonNetwork.IsMasterClient)
        {
            Analytics.CustomEvent("Map started Info analytic", new Dictionary<string, object>
            { 
                { "Mapname ", CreateRoomManager.Instance.currentMapData.mapName },
                { "Player in the room", PhotonNetwork.CurrentRoom.PlayerCount }
            });
        }

        Analytics.CustomEvent("Character selected by player", new Dictionary<string, object>
        { 
            { "Character name", CustomizePlayer.Instance.currentCharacter.prefabName },
        });

    }

    void Update()
    {
        if (currentPlayer != null)
        {
            if (currentPlayer.transform.position.y < minYFallAble)
            {
                currentPlayer.transform.position = GetSpawnPosition();
            }
        }

        if (PhotonNetwork.IsMasterClient && !isGameEnd)
        {
            currentTimeCount -= Time.deltaTime;

            if (Input.GetKeyDown(KeyCode.W))
            {
                currentTimeCount = 1;
            }

            if (currentTimeCount <= 0)
            {
                isGameEnd = true;

                if (!PhotonNetwork.OfflineMode)
                {
                    PhotonNetwork.CurrentRoom.CustomProperties[GameProperty.IS_GAME_WAS_END] = isGameEnd;
                    PhotonNetwork.CurrentRoom.SetCustomProperties(PhotonNetwork.CurrentRoom.CustomProperties);
                }

                OnGameEnd();
            }

            if (!PhotonNetwork.OfflineMode)
            {
                PhotonNetwork.CurrentRoom.CustomProperties[GameProperty.ROOM_TIME_COUNT] = currentTimeCount;
                PhotonNetwork.CurrentRoom.SetCustomProperties(PhotonNetwork.CurrentRoom.CustomProperties);
            }
        }

        gameTimeCountText.text = ":" + Mathf.Round((float) currentTimeCount);
    }

    IEnumerator DisplayPing()
    {
        WaitForSecondsRealtime wait = new WaitForSecondsRealtime(5);

        while (true)
        {
            pingRateText.text = "Ping:" + PhotonNetwork.GetPing();
            yield return wait;
        }

    }

    IEnumerator DisplayFPS()
    {
        WaitForSecondsRealtime wait = new WaitForSecondsRealtime(2);

        while (true)
        {
            fpsRateText.text = "Fps:" + Mathf.RoundToInt(1f / Time.deltaTime);
            yield return wait;
        }

    }

    public void OnLocalPlayerDie(string killerName)
    {
        itemSelector.gameObject.SetActive(false);
        sceneListener.transform.SetParent(null);

        if (killerName != null)
        {
            TextMeshProUGUI textKiller = killerReport.GetComponentInChildren<TextMeshProUGUI>();

            if (textKiller != null)
            {
                textKiller.text = "Killed by " + killerName;
            }
        }

        killerReport.SetActive(true);
        spawnBtn.SetActive(true);

        Analytics.CustomEvent("Player died Count");
    }

    Vector3 GetSpawnPosition()
    {
        if (spawnPoints.Count <= 0)
        {
            Debug.LogError("No spawn point detected");
            return Vector3.zero;
        }

        List<SpawnPoint> currentSpawnPoints = spawnPoints;

        for (int i = 0; i < currentSpawnPoints.Count; i++)
        {
            int randomSpawnIndex = Random.Range(0, currentSpawnPoints.Count - 1);

            SpawnPoint targetSpawnPoint = spawnPoints[randomSpawnIndex];

            if (targetSpawnPoint.IsAbleToSpawn())
            {
                return targetSpawnPoint.transform.position + Vector3.right * Random.Range(-2.5f, 2.5f);
            }
            else
            {
                if (currentSpawnPoints.Count >= 2)
                {
                    currentSpawnPoints.Remove(targetSpawnPoint);
                }
                else //if no spawn point left just spawn it
                {
                    return targetSpawnPoint.transform.position + Vector3.right * Random.Range(-2.5f, 2.5f);
                }

            }
        }

        return Vector3.zero;
    }

    void OnGameWasEnd()
    {
        isGameEnd = true;
        sceneListener.transform.SetParent(null);

        sceneUi.SetActive(false);
        endGameUi.SetActive(true);

        if (FireBaseDatabaseManager.Instance.connectWithDatabase)
            FireBaseDatabaseManager.Instance.AddUserPlayTime();
        else
        {
            PlayerPrefs.SetInt(UserPrefsProperty.PlayerPlayTimes, PlayerPrefs.GetInt(UserPrefsProperty.PlayerPlayTimes) + 1);
        }
    }

    #region UI

    public void OnBtnNext()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        GameScoreManager.Instance.endGameUi.SetActive(false);
        facebookReport.SetActive(true);
    }

    public void OnBtnBackToMenu()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel("MenuScene");

        if (AdmobManager.Instance != null)
            AdmobManager.Instance.ShowFullscreenAd(AdmobManager.FullscreenAdSelected.EndMatchFullscreenAd);

        if (!isGameEnd)
        {
            Analytics.CustomEvent("Player Exit Between Game");
        }
    }

    public void OnBtnSpawn()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        killerReport.SetActive(false);
        spawnBtn.SetActive(false);

        currentSpawnPos = GetSpawnPosition();

        currentPlayer = PhotonNetwork.Instantiate(CHARACTERS_FOLDER_NAME + playerPrefab.name,
            currentSpawnPos, Quaternion.identity, 0);

        CameraFollow2D.Instance.SetTarget(currentPlayer);
        CameraFollow2D.Instance.isActiveOffset = true;

        sceneListener.transform.SetParent(currentPlayer.transform, false);
        sceneListener.transform.localPosition = new Vector3(0, 0, 0);
    }

    #endregion

    #region  Pun callback

    public override void OnRoomPropertiesUpdate(Hashtable propertiesThatChanged)
    {
        if (isGameEnd)
        {
            return;
        }

        object newData;
        if (propertiesThatChanged.TryGetValue(GameProperty.ROOM_TIME_COUNT, out newData))
        {
            if (!PhotonNetwork.IsMasterClient)
                currentTimeCount = (double) newData;
        }
        if (propertiesThatChanged.TryGetValue(GameProperty.IS_GAME_WAS_END, out newData))
        {
            if (!PhotonNetwork.IsMasterClient)
            {
                isGameEnd = (bool) newData;

                if (isGameEnd)
                {
                    OnGameEnd();
                }
            }
        }
    }

    #endregion

}