﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameAudioInfo 
{
    public AudioClip audioClip;
    [Range(0, 5)] public float audioVolume = 1f;
}
