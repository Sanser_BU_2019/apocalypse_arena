﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow2D : MonoBehaviour
{
    [SerializeField] Transform target;
    public bool isActiveOffset;
    [SerializeField] float dampTime = 10f;
    [SerializeField] float moveSpeed = 10f;
    [SerializeField] float xOffset = 0;
    [SerializeField] float yOffset = 0;
    [SerializeField] Vector2 xClampMinMax = Vector2.zero;
    [SerializeField] Vector2 yClampMinMax = Vector2.zero;

    private float margin = 0;

    Vector3 targetPos;

    private float currentX;

    public static CameraFollow2D Instance;
    public bool IsInstanceExit
    {
        get { return Instance != null; }
    }

    void Awake()
    {
        if (IsInstanceExit)
            gameObject.SetActive(false);
        else
            Instance = this;
    }

    void Start()
    {
        if (target == null)
        {
            gameObject.SetActive(false);
        }

        currentX = xOffset;
    }

    void FixedUpdate()
    {
        if (!target)
        {
            return;
        }

        if (dampTime == 0)
        {
            FocusOnTargetImmediately();
        }
        else
        {
            MoveSmooth();
        }

        transform.position = targetPos;
    }

    void FocusOnTargetImmediately()
    {
        float targetX = target.position.x;
        float targetY = target.position.y;

        targetPos = new Vector3(targetX, targetY, transform.position.z);
        transform.position = targetPos;
    }

    void MoveSmooth()
    {
        float targetX = 0;
        float targetY = 0;

        if (isActiveOffset)
        {
            targetX = target.position.x + currentX;
            targetY = target.position.y + yOffset;
        }
        else
        {
            targetX = target.position.x;
            targetY = target.position.y;
        }

        if (Mathf.Abs(transform.position.x - targetX) > margin)
            targetX = Mathf.Lerp(transform.position.x, targetX, dampTime * Time.deltaTime);

        if (Mathf.Abs(transform.position.y - targetY) > margin)
            targetY = Mathf.Lerp(transform.position.y, targetY, dampTime * Time.deltaTime);

        float clampX = Mathf.Clamp(targetX, xClampMinMax.x, xClampMinMax.y);
        float clampY = Mathf.Clamp(targetY, yClampMinMax.x, yClampMinMax.y);

        targetPos = Vector3.MoveTowards(transform.position, new Vector3(clampX, clampY, -10), moveSpeed * Time.deltaTime);
    }

    public void ConvertX(bool value)
    {
        if (value)
        {
            currentX = -1 * xOffset;
        }
        else
        {
            currentX = xOffset;
        }
    }

    public void SetTarget(GameObject value)
    {
        gameObject.SetActive(true);

        if (value != null)
        {
            target = value.transform;
            FocusOnTargetImmediately();
        }
        else
            target = null;

    }

}