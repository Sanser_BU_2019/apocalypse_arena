﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class ItemBase : MonoBehaviourPun
{
    [SerializeField] bool dontDestroy = false;
    public bool ignoreThisitem = false;
    public enum ItemType
    {
        Weapon,
        Medic,
        Special,
        Looting,
        other
    }

    [SerializeField] Object sentData = null;
    public Object GetSentData { get { return sentData; } }

    [SerializeField] ItemType itemType = ItemType.Weapon;
    public ItemType GetItemType { get { return itemType; } }

    [SerializeField] float floatingRange = 0.1f;
    [SerializeField] float floatingSpeed = 1f;
    [SerializeField] float floorYOffset = 0.5f;
    [SerializeField] LayerMask floorLayer = 0;
    [SerializeField] public float respawnTime = 20;
    [SerializeField] protected GameAudioInfo lootItemSound = null;

    [Header("Sprites")]
    [SerializeField] Transform spriteTran = null;
    [SerializeField] SpriteRenderer spriteRenderer = null;
    [SerializeField] Sprite highlightSprite = null;

    private Sprite baseSprite;
    private Vector3 startPos;

    protected AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        if (spriteRenderer != null)
            baseSprite = spriteRenderer.sprite;

        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, Mathf.Infinity, floorLayer);

        if (hit == true)
        {
            transform.position = new Vector3(hit.point.x, hit.point.y, 0) + Vector3.up * floorYOffset;
        }

        if (spriteTran != null)
            startPos = spriteTran.position;
    }

    private void FixedUpdate()
    {
        float x = startPos.x;
        float y = floatingRange * Mathf.Sin(Time.time * floatingSpeed) + startPos.y;
        float z = startPos.z;

        if (spriteTran != null)
            spriteTran.position = new Vector3(x, y, z);
    }

    public void HighlightActive(bool value)
    {
        if (value && highlightSprite != null && spriteRenderer != null)
        {
            spriteRenderer.sprite = highlightSprite;
        }
        else if (spriteRenderer != null)
        {
            spriteRenderer.sprite = baseSprite;
        }
    }

    public void DestroyThisItem()
    {
        if (dontDestroy) return;

        if (PhotonNetwork.OfflineMode)
        {
            RpcDestroyThis();
        }
        else
        {
            photonView.RPC("RpcDestroyThis", RpcTarget.All);
        }
    }

    [PunRPC]
    void RpcDestroyThis()
    {
        if (lootItemSound.audioClip != null)
            audioSource.PlayOneShot(lootItemSound.audioClip, lootItemSound.audioVolume);

        GetComponent<Collider2D>().enabled = false;
        GetComponentInChildren<SpriteRenderer>().enabled = false;
        ignoreThisitem = true;

        Destroy(gameObject, 3);

        if (respawnTime <= 0) return;

        if (PhotonNetwork.IsMasterClient)
        {
            switch (itemType)
            {
                case ItemType.Weapon:
                    SceneAiManager.Instance.SpawnNewWeapon(gameObject.name, transform.position, respawnTime);
                    break;

                case ItemType.Medic:
                    SceneAiManager.Instance.SpawnNewMedic(transform.position, respawnTime);
                    break;
            }

        }
    }
}