﻿using System;
using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FacebookScoreReportManager : MonoBehaviour
{
    [SerializeField] GameObject mainUi = null;
    [SerializeField] TextMeshProUGUI nickNameText = null;
    [SerializeField] TextMeshProUGUI profileNameText = null;
    [SerializeField] TextMeshProUGUI currentReputationText = null;
    [SerializeField] TextMeshProUGUI reputationToAddText = null;
    [SerializeField] Sprite defaultSprite = null;
    [SerializeField] Image profileImage = null;
    [SerializeField] Button watchAdsBtn = null;

    private int trueScore;
    private int currentUserScore;
    private int visualizeScore;
    private int visualizeCurrentScore;

    private void Start()
    {
        nickNameText.text = PhotonNetwork.NickName;

        if (FireBaseDatabaseManager.Instance != null && FireBaseDatabaseManager.Instance.connectWithDatabase)
        {
            if (FacebookDataHandler.Instance.FirstName != null)
                profileNameText.text = FacebookDataHandler.Instance.FirstName;

            if (FacebookDataHandler.Instance.ProfilePic != null)
                profileImage.sprite = FacebookDataHandler.Instance.ProfilePic;
        }
        else
        {
            profileNameText.text = "Guess";
            profileImage.sprite = defaultSprite;

        }

        if (FireBaseDatabaseManager.Instance.connectWithDatabase)
        {
            visualizeCurrentScore = FireBaseDatabaseManager.Instance.currentUser.userRepAmount;
            currentUserScore = visualizeCurrentScore;
        }

        else
        {
            visualizeCurrentScore = PlayerPrefs.GetInt(UserPrefsProperty.PlayerRep);
            currentUserScore = visualizeCurrentScore;
            print(visualizeCurrentScore);
        }

        GameplayManager.Instance.OnGameEnd += OnGameWasEnd;
    }

    private void Update()
    {
        if (mainUi.activeInHierarchy)
        {
            visualizeScore = (int) Mathf.MoveTowards(visualizeScore, trueScore, (trueScore / 2) * Time.deltaTime);
            visualizeCurrentScore = (int) Mathf.MoveTowards(visualizeCurrentScore, currentUserScore + trueScore, (Mathf.Abs(trueScore) / 2) * Time.deltaTime);

            VisualizeAllScore();
        }

    }

    void VisualizeAllScore()
    {
        reputationToAddText.text = $"+{visualizeScore}";
        currentReputationText.text = $"REP:{visualizeCurrentScore}";
    }

    void OnGameWasEnd()
    {
        trueScore = GameScoreManager.Instance.GetCurrentLocalPlayerScore;
    }

    #region Ui Reference

    public void OnBtnGetMoreReputation()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        AdmobManager.Instance.ShowRewardedAd(AdmobManager.RewardedAdSelected.EndMatchAddScoreRewarded, RewardCallBack);
    }

    void RewardCallBack(object sender, Reward args)
    {
        trueScore = Mathf.RoundToInt((float) trueScore * 1.5f);
        watchAdsBtn.interactable = false;

        if (FireBaseDatabaseManager.Instance.connectWithDatabase)
            FireBaseDatabaseManager.Instance.SetReputation(currentUserScore + trueScore);
        else
        {
            PlayerPrefs.SetInt(UserPrefsProperty.PlayerRep, currentUserScore + trueScore);
        }
    }

    #endregion
}