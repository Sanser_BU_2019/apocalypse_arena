﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScoreReport : MonoBehaviour
{
    [SerializeField] protected TextMeshProUGUI nameText;
    [SerializeField] protected TextMeshProUGUI scoreText;
    [SerializeField] protected Image bgImage;

    public int currentReportScore;

    public string ownerName;

    public void SetReportScore(int score)
    {
        currentReportScore = score;
        scoreText.text = score.ToString();
    }

    public void SetReportName(string scoreRank, string name)
    {
        if (name == "")
        {
            nameText.text = scoreRank + "." + ownerName;
        }
        else
        {
            ownerName = name;
            nameText.text = scoreRank + "." + name;
        }

    }

    public void SetReportName(string name)
    {
        nameText.text = ownerName;
    }

    public void SetReportLocalOwner()
    {
        bgImage.color = new Color(bgImage.color.r, bgImage.color.g, bgImage.color.b, 0.9f);
    }
}