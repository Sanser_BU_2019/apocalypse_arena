﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class SpawnPoint : MonoBehaviourPun
{
    List<Transform> contactPlayers = new List<Transform>();

    private void Start()
    {
        gameObject.tag = "SpawnPoint";
    }

    public bool IsAbleToSpawn()
    {
#if UNITY_EDITOR
        if (!gameObject.activeSelf)
        {
            return false;
        }
#endif

        for (int i = 0; i < contactPlayers.Count; i++)
        {
            if (contactPlayers[i] == null)
            {
                contactPlayers.RemoveAt(i);
            }
        }

        if (contactPlayers.Count > 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PlayerBase player = other.gameObject.GetComponent<PlayerBase>();

            if (player != null && !contactPlayers.Contains(player.transform))
            {
                contactPlayers.Add(other.gameObject.transform);
            }
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PlayerBase player = other.gameObject.GetComponent<PlayerBase>();

            if (player != null && contactPlayers.Contains(player.transform))
            {
                contactPlayers.Remove(other.gameObject.transform);
            }
        }
    }
}