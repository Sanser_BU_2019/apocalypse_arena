﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class LootableObject : ItemBase
{
    [Space]
    [SerializeField] SpriteRenderer spriteRend = null;
    [SerializeField] Sprite openSprite = null;
    [SerializeField] Sprite closeSprite = null;
    [SerializeField] float delayTime = 20;
    [SerializeField] GameObject[] lootItem = null;

    float delayTimer;
    bool isOpened;

    private void Awake()
    {
        delayTimer = delayTime;
    }

    private void Update()
    {
        if (!PhotonNetwork.IsMasterClient) return;

        if (isOpened)
        {
            delayTimer -= Time.deltaTime;

            if (delayTimer <= 0)
            {
                ResetLoot();
            }
        }
    }

    public void OpenLoot()
    {
        if (isOpened) return;

        if (PhotonNetwork.OfflineMode)
        {
            RPCOpenLoot();
        }
        else
        {
            photonView.RPC("RPCOpenLoot", RpcTarget.AllViaServer);
        }
    }

    public void ResetLoot()
    {
        if (PhotonNetwork.OfflineMode)
        {
            RPCResetLoot();
        }
        else
        {
            photonView.RPC("RPCResetLoot", RpcTarget.All);
        }
    }

    [PunRPC]
    void RPCOpenLoot()
    {
        isOpened = true;
        delayTimer = delayTime;
        spriteRend.sprite = openSprite;
        ignoreThisitem = true;

        if (lootItemSound.audioClip != null)
            audioSource.PlayOneShot(lootItemSound.audioClip, lootItemSound.audioVolume);

        if (PhotonNetwork.OfflineMode)
        {
            GameObject randLoot = lootItem[Random.Range(0, lootItem.Length)];
            randLoot.GetComponent<ItemBase>().respawnTime = 0;
            Instantiate(randLoot, transform.position + Vector3.right * Random.Range(-2, 2) + Vector3.up * 2, Quaternion.identity);
        }
        else if (PhotonNetwork.IsMasterClient)
        {
            GameObject randLoot = lootItem[Random.Range(0, lootItem.Length)];
            randLoot.GetComponent<ItemBase>().respawnTime = 0;
            PhotonNetwork.InstantiateSceneObject("Item/" + randLoot.name,
                transform.position + Vector3.right * Random.Range(-2, 2) + Vector3.up * 2, Quaternion.identity, 0);
        }

    }

    [PunRPC]
    void RPCResetLoot()
    {
        ignoreThisitem = false;
        spriteRend.sprite = closeSprite;
        isOpened = false;
    }
}