﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerScoreReportEndGame : PlayerScoreReport
{
    [SerializeField] TextMeshProUGUI rankText = null;

    public void SetFinalScoreReport(string ownerName, int rank, int score)
    {
        nameText.text = ownerName;
        scoreText.text = score.ToString();
        rankText.text = rank.ToString();
    }
}
