﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiOnWorld : MonoBehaviour
{
    private RectTransform thisRect;

    void Start()
    {
        thisRect = GetComponent<RectTransform>();
    }

    private void Update()
    {
        thisRect.eulerAngles = new Vector3(0, 0, 0);
    }
}