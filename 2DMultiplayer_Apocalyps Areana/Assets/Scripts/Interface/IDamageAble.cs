﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;

public interface IDamageAble
{
    void TakeDamage(float value, string killerName, string killerCharObjName, Player playerKiller);

    bool CheckIsDied();
}
