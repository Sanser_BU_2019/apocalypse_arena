﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class AttackCollider : MonoBehaviour
{
    Base2DAi baseAi;

    private void Awake()
    {
        baseAi = GetComponentInParent<Base2DAi>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PhotonView playerView = other.GetComponent<PhotonView>();

            if (playerView != null)
            {
                if (playerView.IsMine)
                {
                    PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();

                    if (playerHealth != null && playerHealth.GetCurrentHealth > 0)
                    {
                        playerHealth.TakeDamage(baseAi.GetAttackDamage, "Zombie", transform.parent.gameObject.name, null);
                    }
                }
            }
        }
    }
}