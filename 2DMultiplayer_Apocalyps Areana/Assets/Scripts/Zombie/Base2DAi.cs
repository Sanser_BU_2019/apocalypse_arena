﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Base2DAi : MonoBehaviourPun
{
    public enum AiState
    {
        StandStill,
        ChaseTarget,
        AttackTarget,
        BackToStartPos
    }

    AiState aiState = AiState.StandStill;

    public GameObject target;

    public bool enableMove = true;
    [SerializeField] float moveSpeed = 3;
    [SerializeField] float breakDistance = 1f;
    [SerializeField] float stopChasingDistance = 10f;

    [Header("Jump")]
    public bool enableJump = true;
    [SerializeField] float jumpForce = 500;
    [SerializeField] float jumpDelay = 0.25f;
    [SerializeField] float floorDetectRadius = 0.5f;
    [SerializeField] Vector2 floorDetectOffset = Vector2.zero;
    [SerializeField] LayerMask jumpAbleLayer = 0;

    [Header("Attack")]
    public bool enableAttack = true;
    [SerializeField] float attackDamage = 5f;
    [SerializeField] float attackDelay = 2f;
    [SerializeField] Animator attackCol = null;

    // this will call on ObstacleDetectCollider
    [HideInInspector] public bool isFaceOnObstacle;
    public float GetAttackDamage
    {
        get { return attackDamage; }
    }

    Animator animator;
    Rigidbody2D rb2D;
    float direction = 1;
    float currentHealth;
    float jumpDelayTimeCount;
    float attackDelayTimeCount;
    bool isLockMovement;
    Vector3 startPos;

    List<GameObject> collisionOtherAi = new List<GameObject>();

    void Start()
    {
        aiState = AiState.StandStill;
        jumpDelayTimeCount = 0;
        attackDelayTimeCount = 0;
        isLockMovement = false;

        startPos = transform.position;

        gameObject.name = gameObject.name + photonView.ViewID;

        animator = GetComponent<Animator>();
        rb2D = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        if (!PhotonNetwork.IsMasterClient) return;

        attackDelayTimeCount -= Time.deltaTime;
        jumpDelayTimeCount -= Time.deltaTime;

        if (target == null)
        {
            OnStateBackToStartPos();
            return;
        }

        PlayerHealth player = target.GetComponent<PlayerHealth>();

        if (player != null)
        {
            if (player.GetCurrentHealth <= 0)
            {
                target = null;
                return;
            }
        }

        Vector3 targetPos = target.transform.position;
        float distance = Vector2.Distance(targetPos, (Vector2) transform.position);

        switch (aiState)
        {
            case AiState.StandStill:
                OnStateStandStill(distance);
                break;

            case AiState.ChaseTarget:
                OnStateChaseTarget(targetPos, distance, enableAttack);
                break;

            case AiState.AttackTarget:
                OnStateAttack(targetPos, distance);
                break;

        }
    }

    private void Update()
    {
        if (!PhotonNetwork.IsMasterClient) return;

        if (collisionOtherAi.Count > 0)
        {
            isLockMovement = true;
        }
        else
        {
            isLockMovement = false;
        }
    }

    public void SetTarget(GameObject newTarget)
    {
        target = newTarget;
    }

    void OnStateStandStill(float currentDistance)
    {
        animator.SetBool("IsRunning", false);

        if (target != null)
        {
            if (currentDistance > breakDistance || !isLockMovement)
            {
                aiState = AiState.ChaseTarget;
            }
        }
    }

    void OnStateChaseTarget(Vector3 targetPos, float currentDistance, bool isAbleAttack)
    {
        float xDistance = targetPos.x - transform.position.x;
        if (Mathf.Abs(xDistance) <= breakDistance || isLockMovement)
        {
            if (attackDelayTimeCount <= 0 && isAbleAttack)
            {
                aiState = AiState.AttackTarget;
            }
            else
            {
                aiState = AiState.StandStill;
            }

            return;
        }

        if (currentDistance > stopChasingDistance)
        {
            aiState = AiState.StandStill;
            target = null;
        }

        if (!enableMove)
        {
            return;
        }

        animator.SetBool("IsRunning", true);

        if (targetPos.x > transform.position.x) //face direction
        {
            direction = 1;
            transform.rotation = Quaternion.Euler(0, 1, 0);
        }
        else if (targetPos.x < transform.position.x)
        {
            direction = -1;
            transform.rotation = Quaternion.Euler(0, 179, 0);
        }

        if (Mathf.Abs(xDistance) > 0.1f) //Move
        {
            transform.position += Vector3.right * direction * moveSpeed * Time.deltaTime;

            if (isFaceOnObstacle && jumpDelayTimeCount <= 0 && enableJump) //Jump
            {
                Collider2D floor = Physics2D.OverlapCircle((Vector2) transform.position + floorDetectOffset, floorDetectRadius, jumpAbleLayer);

                if (floor != null)
                {
                    rb2D.AddForce(Vector3.up * jumpForce);
                    jumpDelayTimeCount = jumpDelay;
                }
            }
        }
    }

    void OnStateAttack(Vector3 targetPos, float currentDistance)
    {
        if (!enableAttack)
        {
            aiState = AiState.StandStill;
            return;
        }

        isLockMovement = true;

        if (targetPos.x > transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 1, 0);
        }
        else if (targetPos.x < transform.position.x)
        {
            transform.rotation = Quaternion.Euler(0, 179, 0);
        }

        if (PhotonNetwork.OfflineMode)
        {
            RpcCallAttackTrigger();
        }
        else
        {
            photonView.RPC("RpcCallAttackTrigger", RpcTarget.All);
        }

        attackDelayTimeCount = attackDelay;
        aiState = AiState.StandStill;

        Invoke("AbleRunning", 0.75f);
    }

    void OnStateBackToStartPos()
    {
        float distance = startPos.x - transform.position.x;

        if (Mathf.Abs(distance) > 0 && Mathf.Abs(distance) < breakDistance)
        {
            animator.SetBool("IsRunning", false);
        }
        else
        {
            OnStateChaseTarget(startPos, distance, false);
        }

    }

    void AbleRunning()
    {
        isLockMovement = false;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, stopChasingDistance);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position + (Vector3) floorDetectOffset, floorDetectRadius);
    }

    [PunRPC]
    public void RpcCallAttackTrigger()
    {
        animator.SetTrigger("TriggerAttack");
        attackCol.SetTrigger("StartAnim");
    }
}