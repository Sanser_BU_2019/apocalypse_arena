﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class ObstacleDetectCollider : MonoBehaviour
{
    Base2DAi baseAi;

    List<GameObject> obstacles = new List<GameObject>();

    private void Awake()
    {
        baseAi = GetComponentInParent<Base2DAi>();
    }

    private void Update()
    {
        if (obstacles.Count > 0)
        {
            baseAi.isFaceOnObstacle = true;
        }
        else
        {
            baseAi.isFaceOnObstacle = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!PhotonNetwork.IsMasterClient) return;

        if (other.gameObject.CompareTag("Floor") || other.gameObject.CompareTag("Zombie"))
        {
            obstacles.Add(other.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!PhotonNetwork.IsMasterClient) return;

        if (other.gameObject.CompareTag("Floor") || other.gameObject.CompareTag("Zombie"))
        {
            obstacles.Remove(other.gameObject);
        }
    }
}