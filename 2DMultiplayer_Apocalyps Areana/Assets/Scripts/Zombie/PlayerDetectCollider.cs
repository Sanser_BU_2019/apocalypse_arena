﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerDetectCollider : MonoBehaviour
{
    Base2DAi baseAi;

    private void Awake()
    {
        baseAi = GetComponentInParent<Base2DAi>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!PhotonNetwork.IsMasterClient) return;

        if (other.gameObject.CompareTag("Player") && baseAi.target == null)
        {
            baseAi.target = other.gameObject;
        }
    }
}