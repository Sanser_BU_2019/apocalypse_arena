﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Analytics;

public class EnemyBase : MonoBehaviourPun, IDamageAble
{
    [Header("Stats")]
    [SerializeField] bool isRandomHealth = true;
    [SerializeField] float health = 50;
    [SerializeField] float maxHealth = 70;
    [SerializeField] int score = 10;
    [SerializeField] GameObject worldUi = null;
    [SerializeField] Image healthFillImage = null;
    [SerializeField] GameObject deadSprite = null;
    [SerializeField] GameObject bloodEffect = null;

    private Player lastKiller;
    private Vector3 startPos;
    private Base2DAi ai;
    private float trueHealth;
    private float currentHealth;

    void Start()
    {
        ai = GetComponent<Base2DAi>();

        startPos = transform.position;
        deadSprite.SetActive(false);
        worldUi.SetActive(false);

        if (isRandomHealth)
        {
            trueHealth = Random.Range(health, maxHealth);
        }
        else
        {
            trueHealth = health;
        }

        if (PhotonNetwork.OfflineMode)
        {
            RpcUpdateHealthVisualized(trueHealth);
        }
        else if (PhotonNetwork.IsMasterClient)
        {
            photonView.RPC("RpcUpdateHealthVisualized", RpcTarget.All, trueHealth);
        }
    }

    public void TakeDamage(float value, string killerName, string killerCharObjName, Player playerKiller)
    {
        if (PhotonNetwork.OfflineMode)
        {
            RpcTakeDamage(value, killerName, killerCharObjName);
        }
        else
        {
            photonView.RPC("RpcTakeDamage", RpcTarget.All, value, killerName, killerCharObjName);
        }

        if (playerKiller != null)
        {
            lastKiller = playerKiller;
        }
    }

    void OnDie()
    {
        if (PhotonNetwork.OfflineMode)
        {
            RpcOnDie();
        }
        else
        {
            photonView.RPC("RpcOnDie", RpcTarget.All);
        }
    }

    public bool CheckIsDied()
    {
        if (currentHealth <= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    [PunRPC]
    public void RpcTakeDamage(float value, string killerName, string killerCharObjName)
    {
        if (currentHealth <= 0)
        {
            return;
        }

        currentHealth -= value;
        healthFillImage.fillAmount = currentHealth / trueHealth;

        GameObject obj = Instantiate(bloodEffect, transform.position, Quaternion.identity);
        Destroy(obj, 3);

        worldUi.SetActive(true);

        if (PhotonNetwork.IsMasterClient)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

            foreach (var player in players)
            {
                if (player.name == killerCharObjName)
                {
                    ai.SetTarget(player);
                }
            }

            if (currentHealth <= 0)
            {
                OnDie();
            }
        }
    }

    [PunRPC]
    public void RpcOnDie()
    {
        ai.enableMove = false;
        ai.enableJump = false;
        ai.enableAttack = false;

        GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        GetComponent<Rigidbody2D>().isKinematic = true;
        GetComponent<Collider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;

        worldUi.SetActive(false);
        deadSprite.SetActive(true);
        Destroy(gameObject, 5);

        if (PhotonNetwork.IsMasterClient)
        { 
            Analytics.CustomEvent("Zombie died count");
        }

        if (PhotonNetwork.IsMasterClient || PhotonNetwork.OfflineMode)
        {
            
            SceneAiManager.Instance.SpawnNewEnemy("Zombie_Normal", startPos, 35f);
        }
        if (lastKiller != null && lastKiller == PhotonNetwork.LocalPlayer)
        {
            GameScoreManager.Instance.AddPlayerScore(score);
        }

    }

    [PunRPC]
    public void RpcUpdateHealthVisualized(float targetTrueHealth)
    {
        trueHealth = targetTrueHealth;
        currentHealth = trueHealth;

        healthFillImage.fillAmount = currentHealth / trueHealth;
    }
}