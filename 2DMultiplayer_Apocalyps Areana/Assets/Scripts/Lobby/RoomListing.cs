﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

using Hashtable = ExitGames.Client.Photon.Hashtable;

public class RoomListing : MonoBehaviourPunCallbacks
{
    [SerializeField] Transform roomContainer = null;
    [SerializeField] GameObject roomPrefab = null;
    [SerializeField] GameObject nonRoomReportPrefab = null;

    private List<GameRoom> currentGameRooms = new List<GameRoom>();

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        foreach (RoomInfo room in roomList)
        {
            if (room.RemovedFromList || !room.IsVisible || !room.IsOpen)
            {
                DeleteRoom(room);
            }
            else
            {
                AddRoom(room);
            }
        }

        if(currentGameRooms.Count <= 0)
        {
            nonRoomReportPrefab.gameObject.SetActive(true);
        }
        else
        {
            nonRoomReportPrefab.gameObject.SetActive(false);
        }

    }

    public void AddRoom(RoomInfo room)
    {
        string roomMapName = (string)room.CustomProperties[RoomProperty.MapName];

        for (int i = 0; i < currentGameRooms.Count; ++i)
        {
            if (currentGameRooms[i].GetRoomId == room.Name) //if room already create just update it
            {
                currentGameRooms[i].SetRoomInfo(room.Name, roomMapName,room.PlayerCount, room.MaxPlayers);
                return;
            }
        }

        GameObject obj = Instantiate(roomPrefab, Vector2.zero, Quaternion.identity);
        obj.transform.SetParent(roomContainer, false);

        GameRoom gameRoom = obj.GetComponent<GameRoom>();

        if (gameRoom != null)
        {
            gameRoom.SetRoomInfo(room.Name, roomMapName, room.PlayerCount, room.MaxPlayers);
            currentGameRooms.Add(gameRoom);
        }
    }

    public void DeleteRoom(RoomInfo room)
    {
        if (currentGameRooms.Count < 1) return;

        for (int i = 0; i < currentGameRooms.Count; ++i)
        {
            if (currentGameRooms[i].GetRoomId == room.Name)
            {
                Destroy(currentGameRooms[i].gameObject);
                currentGameRooms.RemoveAt(i);
            }
        }
    }
}