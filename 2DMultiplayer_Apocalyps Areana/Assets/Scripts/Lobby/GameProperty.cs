﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameProperty
{
    public const string IS_PLAYER_READY = "isPlayerReady";
    public const string IS_PLAYER_LOADED_LEVEL = "isPlayerLoadLevel";

    public const string ROOM_TIME_COUNT = "roomTimeCount";
    public const string IS_GAME_WAS_END = "isGameWasEnd";

    public const string PLAYER_SCORE = "PlayerScore";
}