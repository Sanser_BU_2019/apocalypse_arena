﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CreateRoomManager : MonoBehaviourPunCallbacks
{
    [SerializeField] TMP_InputField joinRoomField = null;
    [SerializeField] TextMeshProUGUI createRoomIdText = null;
    [SerializeField] TextMeshProUGUI mapInfoText = null;
    [SerializeField] TextMeshProUGUI mapNameText = null;
    [SerializeField] Image mapImage = null;

    private const int MAXIMUM_ROOM_ID = 99999;
    private const int MININUM_ROOM_ID = 10000;

    private int myCreateRoomId;

    private MapData[] gameMapData;
    public MapData currentMapData;
    private int currentMapIndex;
    private RoomOptions roomOptions;
    private List<RoomInfo> currentRoomList = new List<RoomInfo>();

    public static CreateRoomManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        gameMapData = Resources.LoadAll<MapData>("MapData");

        if (gameMapData.Length < 1)
        {
            Debug.LogError("no game map find");
        }
        else
        {
            SetMapVisualize(gameMapData[currentMapIndex]);
        }

        RandomMyRoomID();
    }

    void RandomMyRoomID()
    {
        myCreateRoomId = Random.Range(MININUM_ROOM_ID, MAXIMUM_ROOM_ID);
        createRoomIdText.text = "ID : " + myCreateRoomId;

        if (IsRoomAlreadyExit())
        {
            RandomMyRoomID();
        }
    }

    void SetMapVisualize(MapData targetMap)
    {
        mapInfoText.text = $"Map Player : {targetMap.maxPlayer}     Time : {targetMap.gameTime}";
        mapNameText.text = $"{targetMap.mapName}";
        mapImage.sprite = targetMap.mapImage;

        currentMapData = targetMap;
    }

    bool IsRoomAlreadyExit()
    {
        for (int i = 0; i < currentRoomList.Count; i++)
        {
            if (myCreateRoomId.ToString() == currentRoomList[i].Name)
            {
                return true;
            }
        }

        return false;
    }

    #region Ui

    public void OnBtnNextMapSelection()
    {
        currentMapIndex++;

        if (currentMapIndex > gameMapData.Length - 1)
        {
            currentMapIndex = 0;
        }

        SoundManager.Instance.PlayBtnSound();
        SetMapVisualize(gameMapData[currentMapIndex]);
    }

    public void OnBtnPreviousMapSelection()
    {
        currentMapIndex--;

        if (currentMapIndex < 0)
        {
            currentMapIndex = gameMapData.Length - 1;
        }

        SoundManager.Instance.PlayBtnSound();
        SetMapVisualize(gameMapData[currentMapIndex]);
    }

    #endregion

    #region Pun Callback

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        currentRoomList = roomList;
    }

    public override void OnJoinedRoom()
    {
        RandomMyRoomID();
    }

    #endregion

    #region UI

    public void OnBtnJoinRoom()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        if (joinRoomField.text.Length != 5)
        {
            return;
        }

        PhotonNetwork.JoinOrCreateRoom(joinRoomField.text, roomOptions, TypedLobby.Default);
    }

    public void OnBtnCreateRoom()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        MenuManager.Instance.createRoomUi.SetActive(false);

        roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = (byte) currentMapData.maxPlayer;

        roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable();
        roomOptions.CustomRoomProperties.Add(RoomProperty.MapName, currentMapData.mapName);
        roomOptions.CustomRoomProperties.Add(RoomProperty.LevelLoadName, currentMapData.mapSceneName);

        roomOptions.CustomRoomPropertiesForLobby = new string[]
        {
            RoomProperty.MapName
        };

        PhotonNetwork.CreateRoom(myCreateRoomId.ToString(), roomOptions, TypedLobby.Default);
        MenuManager.Instance.inRoomUi.SetActive(true);
    }

    #endregion

}