﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameRoom : MonoBehaviourPunCallbacks
{
    [SerializeField] TextMeshProUGUI idText = null;
    [SerializeField] TextMeshProUGUI mapText = null;
    [SerializeField] TextMeshProUGUI playerCountText = null;
    [SerializeField] Button joinGameBtn = null;

    private string roomId;
    private string currentPlayer;
    private string maxPlayer;

    public string GetRoomId { get { return roomId; } }

    private void Start()
    {
        joinGameBtn.onClick.AddListener(() => OnBtnJoinRoomInList());
    }

    void OnBtnJoinRoomInList()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        PhotonNetwork.JoinRoom(roomId);
        MenuManager.Instance.roomlistUi.SetActive(false);
    }

    public void SetRoomInfo(string id, string mapName, int countPlayer, int roomMaxPlayers)
    {
        roomId = id;
        currentPlayer = countPlayer.ToString();
        maxPlayer = roomMaxPlayers.ToString();

        playerCountText.text = string.Format("{0} / {1}", currentPlayer, roomMaxPlayers);
        idText.text = roomId;
        mapText.text = mapName;
    }

}