﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSelection : MonoBehaviour
{
    [SerializeField] GameObject targetPrefab = null;

    public string prefabName
    {
        get { return targetPrefab.name; }
    }
}