﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviourPunCallbacks
{
    [SerializeField] TextMeshProUGUI connectStateText = null;

    [Header("All UI Header")]
    [SerializeField] public GameObject initialUi = null;
    [SerializeField] public GameObject facebookUi = null;
    [SerializeField] public GameObject nameConfigUi = null;
    [SerializeField] public GameObject mainMenuUi = null;
    [SerializeField] public GameObject inRoomUi = null;

    [Header("Btn Response Ui")]
    [SerializeField] public GameObject createRoomUi = null;
    [SerializeField] public GameObject roomlistUi = null;
    [SerializeField] public GameObject customizeUi = null;
    [SerializeField] public GameObject onRandomRoomFailed = null;

    public static MenuManager Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        initialUi.SetActive(true);
        facebookUi.SetActive(false);
        createRoomUi.SetActive(false);
        roomlistUi.SetActive(false);
        nameConfigUi.SetActive(false);
        mainMenuUi.SetActive(false);
        inRoomUi.SetActive(false);
        customizeUi.SetActive(false);
        onRandomRoomFailed.SetActive(false);

        AdmobManager.Instance.RequestBannerView(true);
    }

    private void Update()
    {
        connectStateText.text = "Connection State : " + PhotonNetwork.NetworkClientState + ", Region : " + PhotonNetwork.CloudRegion;
    }

   
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        onRandomRoomFailed.gameObject.SetActive(true);
        mainMenuUi.gameObject.SetActive(false);
    }

    #region UI

    public void OnFieldTypingSound()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayTypingSound();
    }

    public void OnBtnRandomRoom()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        PhotonNetwork.JoinRandomRoom();
    }

    public void OnAnyBtnPlayClickSound()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();
    }

    

    #endregion
}