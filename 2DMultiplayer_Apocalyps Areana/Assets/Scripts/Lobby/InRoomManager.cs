﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

using Hashtable = ExitGames.Client.Photon.Hashtable;

public class InRoomManager : MonoBehaviourPunCallbacks
{
    [Header("Setting")]
    [SerializeField] GameObject playerInRoomPrefab = null;
    [SerializeField] Transform playerInRoomContainer = null;
    [SerializeField] TextMeshProUGUI roomIdText = null;
    [SerializeField] TextMeshProUGUI roomDetailsText = null;
    [SerializeField] Button readyBtn = null;

    public static Button PlayerReadyBtn;

    private const string HOST_BUTTON_TEXT = "Start Game";
    private const string PLAYER_BUTTON_TEXT = "Ready";

    Dictionary<int, GameObject> playerListsInRoom = new Dictionary<int, GameObject>();

    private void Awake()
    {
        PlayerReadyBtn = readyBtn;
    }

    void VisualizeRoomInfo()
    {
        Player hostPlayer = PhotonNetwork.CurrentRoom.Players[PhotonNetwork.CurrentRoom.masterClientId];

        roomIdText.text = "Room ID : " + PhotonNetwork.CurrentRoom.Name;
        roomDetailsText.text = "Hostname : " + hostPlayer.NickName + ", Your name : " + PhotonNetwork.LocalPlayer.NickName;
    }

    private bool IsAllPlayersReady()
    {
        if (!PhotonNetwork.IsMasterClient)
        {
            return false;
        }

        foreach (Player p in PhotonNetwork.PlayerList)
        {
            if (p.CustomProperties.ContainsKey(GameProperty.IS_PLAYER_READY))
            {
                object isPlayerReady;
                if ((bool) p.CustomProperties.TryGetValue(GameProperty.IS_PLAYER_READY, out isPlayerReady))
                {
                    if (!(bool) isPlayerReady)
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    #region Pun Callback

    public override void OnJoinedRoom()
    {
        MenuManager.Instance.mainMenuUi.SetActive(false);
        MenuManager.Instance.inRoomUi.SetActive(true);
        readyBtn.gameObject.SetActive(true);

        VisualizeRoomInfo();

        if (PhotonNetwork.LocalPlayer.IsMasterClient)
        {
            readyBtn.GetComponentInChildren<TextMeshProUGUI>().text = HOST_BUTTON_TEXT;
        }
        else
        {
            readyBtn.GetComponentInChildren<TextMeshProUGUI>().text = PLAYER_BUTTON_TEXT;
        }

        foreach (Player player in PhotonNetwork.PlayerList)
        {
            AddPlayerInRoom(player);
        }

        Hashtable props = new Hashtable
        { { GameProperty.IS_PLAYER_LOADED_LEVEL, false }
        };
        PhotonNetwork.LocalPlayer.SetCustomProperties(props);
    }

    public override void OnLeftRoom()
    {
        foreach (GameObject playerInRoom in playerListsInRoom.Values)
        {
            Destroy(playerInRoom.gameObject);
        }

        playerListsInRoom.Clear();
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        AddPlayerInRoom(newPlayer);
    }

    void AddPlayerInRoom(Player player)
    {
        GameObject playerInRoomObj = Instantiate(playerInRoomPrefab, Vector2.zero, Quaternion.identity);
        playerInRoomObj.transform.SetParent(playerInRoomContainer, false);

        PlayerInWaitingRoom playerInRoom = playerInRoomObj.GetComponent<PlayerInWaitingRoom>();

        if (playerInRoom != null)
        {
            playerInRoom.SetPlayerInfo(player);

            object isPlayerReady;
            if (player.CustomProperties.TryGetValue(GameProperty.IS_PLAYER_READY, out isPlayerReady))
            {
                playerInRoom.SetPlayerReady((bool) isPlayerReady);
            }

        }

        playerListsInRoom.Add(player.ActorNumber, playerInRoomObj);
        StartGameBtnActive(IsAllPlayersReady());
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        Destroy(playerListsInRoom[otherPlayer.ActorNumber].gameObject);
        playerListsInRoom.Remove(otherPlayer.ActorNumber);

        StartGameBtnActive(IsAllPlayersReady());
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber == newMasterClient.ActorNumber)
        {
            VisualizeRoomInfo();
            readyBtn.GetComponentInChildren<TextMeshProUGUI>().text = HOST_BUTTON_TEXT;

            StartGameBtnActive(IsAllPlayersReady());
        }
    }

    public override void OnPlayerPropertiesUpdate(Player target, ExitGames.Client.Photon.Hashtable changedProps)
    {
        GameObject playerInRoomObj;
        if (playerListsInRoom.TryGetValue(target.ActorNumber, out playerInRoomObj))
        {
            object isPlayerReady;
            if (changedProps.TryGetValue(GameProperty.IS_PLAYER_READY, out isPlayerReady))
            {
                playerInRoomObj.GetComponent<PlayerInWaitingRoom>().SetPlayerReady((bool) isPlayerReady);
            }
        }

        StartGameBtnActive(IsAllPlayersReady());
    }

    #endregion

    #region UI Method

    public void OnBtnLeaveRoom()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        PhotonNetwork.LeaveRoom();

        MenuManager.Instance.inRoomUi.SetActive(false);
        MenuManager.Instance.mainMenuUi.SetActive(true);
    }

    private void StartGameBtnActive(bool value)
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        if (PhotonNetwork.IsMasterClient)
        {
            readyBtn.gameObject.SetActive(value);
        }
    }

    #endregion

}