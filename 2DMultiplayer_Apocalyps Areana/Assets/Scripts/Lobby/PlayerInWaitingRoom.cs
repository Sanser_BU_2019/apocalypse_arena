﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

using Hashtable = ExitGames.Client.Photon.Hashtable;

public class PlayerInWaitingRoom : MonoBehaviourPunCallbacks
{
    [SerializeField] TextMeshProUGUI playerInfoText = null;

    [Header("Ready Visualize")]
    [SerializeField] TextMeshProUGUI isReadyText = null;
    [SerializeField] Image readyTextBg = null;
    [SerializeField] Color readyColor = Color.green;
    [SerializeField] Color notReadyColor = Color.red;

    private const string READY_TEXT = "Ready";
    private const string NOT_READY_TEXT = "Not Ready";

    private const string HOST_TEXT = "HOST";
    private const string PLAYER_TEXT = "Ready";

    private Player playerOwner;
    private bool isPlayerReady = false;
    private bool isGameStarted = false;

    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;

        if (PhotonNetwork.LocalPlayer.UserId == playerOwner.UserId)
        {
            isPlayerReady = false;
            Hashtable initialProps = new Hashtable();

            if (PhotonNetwork.IsMasterClient)
            {
                isReadyText.text = HOST_TEXT;
                readyTextBg.color = readyColor;

                initialProps[GameProperty.IS_PLAYER_READY] = true;
                InRoomManager.PlayerReadyBtn.onClick.AddListener(() => OnBtnStartGame());
            }
            else
            {
                initialProps[GameProperty.IS_PLAYER_READY] = isPlayerReady;

                InRoomManager.PlayerReadyBtn.onClick.AddListener(() =>
                {
                    SetPlayerReady(!isPlayerReady);

                    if (SoundManager.Instance.IsExit)
                        SoundManager.Instance.PlayBtnSound();

                    Hashtable props = new Hashtable() { { GameProperty.IS_PLAYER_READY, isPlayerReady } };
                    PhotonNetwork.LocalPlayer.SetCustomProperties(props);
                });
            }

            PhotonNetwork.LocalPlayer.SetCustomProperties(initialProps);
        }
    }

    public void SetPlayerReady(bool value)
    {
        if (playerOwner.ActorNumber == PhotonNetwork.CurrentRoom.MasterClientId)
        {
            isReadyText.text = HOST_TEXT;
            readyTextBg.color = readyColor;
            return;
        }
        isPlayerReady = value;

        isReadyText.text = isPlayerReady ? READY_TEXT : NOT_READY_TEXT;
        readyTextBg.color = isPlayerReady ? readyColor : notReadyColor;
    }

    public void SetPlayerInfo(Player targetPlayer)
    {
        playerOwner = targetPlayer;

        if (playerOwner.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber)
        {
            playerInfoText.text = string.Format("Name: {0} (You)", playerOwner.NickName);
        }
        else
        {
            playerInfoText.text = string.Format("Name: {0}", playerOwner.NickName);
        }

    }

    public void OnBtnStartGame()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        PhotonNetwork.CurrentRoom.IsVisible = false;
        PhotonNetwork.CurrentRoom.IsOpen = false;

        MenuManager.Instance.inRoomUi.SetActive(true);

        if (!isGameStarted)
        {
            isGameStarted = true;
            PhotonNetwork.LoadLevel((string) PhotonNetwork.CurrentRoom.CustomProperties[RoomProperty.LevelLoadName]);
        }
    }

    #region  Pun Callback

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        if (PhotonNetwork.LocalPlayer.ActorNumber == newMasterClient.ActorNumber)
        {
            isReadyText.text = HOST_TEXT;
            readyTextBg.color = readyColor;

            Hashtable props = new Hashtable() { { GameProperty.IS_PLAYER_READY, true } };
            PhotonNetwork.LocalPlayer.SetCustomProperties(props);

            InRoomManager.PlayerReadyBtn.onClick.RemoveAllListeners();
            InRoomManager.PlayerReadyBtn.onClick.AddListener(() => OnBtnStartGame());
        }
    }

    #endregion
}