﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class CustomizePlayer : MonoBehaviour
{
    [SerializeField] Transform VisualizeContent = null;

    CharacterSelection[] allCharacters;
    List<GameObject> allChractersPrefab = new List<GameObject>();

    public CharacterSelection currentCharacter;

    int characterIndex;

    public static CustomizePlayer Instance;

    private void Awake() {
        Instance = this;
    }

    void Start()
    {
        allCharacters = Resources.LoadAll<CharacterSelection>("CharactersSelection");

        if (allCharacters.Length > 0)
        {
            foreach (CharacterSelection c in allCharacters)
            {
                GameObject newObj = Instantiate(c.gameObject);
                newObj.SetActive(false);
                newObj.transform.SetParent(VisualizeContent.transform, false);

                allChractersPrefab.Add(newObj);
            }

            characterIndex = 0;
            SetVisualizeContent(allChractersPrefab[characterIndex]);
        }
    }

    void SetVisualizeContent(GameObject targetCharacter)
    {
        if (currentCharacter != null)
        {
            currentCharacter.gameObject.SetActive(false);
            currentCharacter = null;
        }

        currentCharacter = targetCharacter.GetComponent<CharacterSelection>();

        if (currentCharacter != null)
        {
            currentCharacter.gameObject.SetActive(true);
        }
    }

    #region Ui

    public void OnBtnNextCharacter()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        characterIndex++;

        if (characterIndex > allCharacters.Length - 1)
        {
            characterIndex = 0;
        }

        SetVisualizeContent(allChractersPrefab[characterIndex]);
    }

    public void OnBtnPreviousCharacter()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        characterIndex--;

        if (characterIndex < 0)
        {
            characterIndex = allCharacters.Length - 1;
        }

        SoundManager.Instance.PlayBtnSound();
        SetVisualizeContent(allChractersPrefab[characterIndex]);
    }

    public void OnBtnConfirmCharacter()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        MenuManager.Instance.customizeUi.SetActive(false);
        MenuManager.Instance.mainMenuUi.SetActive(true);
        
        PlayerPrefs.SetString(PlayerProperty.PLAYER_CHARACTER_NAME, currentCharacter.prefabName);
    }

    #endregion
}