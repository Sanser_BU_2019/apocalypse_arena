﻿using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using Photon.Pun;
using Photon.Realtime;
using TMPro;
using UnityEngine;

public class GameInitialManager : MonoBehaviourPunCallbacks
{
    [SerializeField] GameObject pressAnyKeyText = null;
    [SerializeField] TextMeshProUGUI connectionText = null;

    private bool isReadyToGoMenu = false;
    private bool isGoMenu = false;

    private void Awake()
    {
        ConnectToPunServer();
    }

    private void Update()
    {
        if (Input.anyKeyDown && isReadyToGoMenu && !isGoMenu)
        {
            if (FB.IsLoggedIn)
            {
                MenuManager.Instance.nameConfigUi.SetActive(true);
            }
            else
            {
                MenuManager.Instance.facebookUi.SetActive(true);
            }

            MenuManager.Instance.initialUi.SetActive(false);

            isGoMenu = true;
        }
        else
        {
            connectionText.text = "Connection state : " + PhotonNetwork.NetworkClientState;
        }
    }

    ///<Summary>Connect to the pun Server</summary>
    private void ConnectToPunServer()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    ///<Summary>Connect to Lobby When successfully connect Pun Server</summary>
    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to master!!");

        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    ///<Summary>Call when join a Lobby</summary>
    public override void OnJoinedLobby()
    {
        Debug.Log("Connected to Lobby!!");

        isReadyToGoMenu = true;
        pressAnyKeyText.SetActive(true);

    }

}