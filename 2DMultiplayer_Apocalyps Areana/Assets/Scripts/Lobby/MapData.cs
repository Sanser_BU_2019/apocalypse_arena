﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "MapData",menuName = "Data/MapData")]
public class MapData : ScriptableObject
{
    public string mapName;
    public string mapSceneName;
    public int maxPlayer;
    public int gameTime;
    public Sprite mapImage;
}
