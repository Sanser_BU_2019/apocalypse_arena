﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNameConfig : MonoBehaviour
{
    [Header("Header")]
    [SerializeField] GameObject nameAsking = null;
    [SerializeField] GameObject roomMenu = null;

    [Header("Name Field")]
    [SerializeField] TMP_InputField inputNameField = null;
    [SerializeField] Button nameConfirmBtn = null;
    [SerializeField] TextMeshProUGUI nameReportText = null;

    private const float MAXIMUM_NAME_CHARACTERS = 9;
    private const float MINIUM_NAME_CHARACTERS = 4;

    private static string NameWrongRuleMassage =
        string.Format("Only {0} - {1} characters!", MINIUM_NAME_CHARACTERS, MAXIMUM_NAME_CHARACTERS);

    private void Start()
    {
        nameConfirmBtn.interactable = false;
        nameReportText.text = NameWrongRuleMassage;
        inputNameField.text = PhotonNetwork.NickName;
    }

    public void OnFieldNameChange()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayTypingSound();

        if (inputNameField.text.Length >= MINIUM_NAME_CHARACTERS &&
            inputNameField.text.Length <= MAXIMUM_NAME_CHARACTERS)
        {
            nameConfirmBtn.interactable = true;
        }
        else
        {
            nameConfirmBtn.interactable = false;

        }

        if (nameReportText != null)
            nameReportText.text = nameConfirmBtn.interactable ? "" : NameWrongRuleMassage;
    }

    public void OnBtnConfirmName()
    {
        if (SoundManager.Instance.IsExit)
            SoundManager.Instance.PlayBtnSound();

        PhotonNetwork.NickName = inputNameField.text;
        nameAsking.SetActive(false);
        roomMenu.SetActive(true);
    }
}